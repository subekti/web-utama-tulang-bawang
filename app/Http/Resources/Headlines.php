<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Headlines extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
            'id'            => $this->id,
            'judul_artikel' => $this->judul_artikel,
            'tanggal'       => $this->tanggal,
        ];
		
    }
}

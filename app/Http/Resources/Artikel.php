<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Artikel extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->kategori) {
            $kategori =  $this->kategori->kategori;
        } else {
            $kategori = null;
        }
		return [
            'id'            => $this->id,
            'judul_artikel' => $this->judul_artikel,
            'isi_artikel'   => $this->isi_artikel,
            'kategori_id'   => $this->kategori_id,
            'kategori'      => $kategori,
            'caption'       => $this->caption,
            'parent'        => $this->parent,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'tanggal' => date('d F Y', strtotime($this->tanggal)),
            'view'          => $this->view,
            'img_thumb'     => url('assets/images/artikel/'.$this->instansi_id.'/'.$this->img),
        ];
		
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Setting extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
            'nama_kepala' => $this->nama_kepala,
            'kabupaten' => $this->judul_media,
            'provinsi'   => $this->judul_img,
            'foto_kepala' => $this->foto_kepala,
            'img_path'    => url('assets/images/web/'.$this->img),
        ];
		
    }
}
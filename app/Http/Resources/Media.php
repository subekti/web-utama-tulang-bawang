<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Media extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
            'id'          => $this->id,
            'type' 		  => $this->type,
            'judul_media' => $this->judul_media,
            'judul_img'   => $this->judul_img,
            'source'   	  => $this->source,
            'img'         => $this->img,
            'img_path'    => url('assets/images/media/'.$this->img),
        ];
		
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pages extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
            'id' => $this->id,
            'judul' => $this->judul,
            'keyword' => $this->keyword,
            'description' => $this->description,
            'status' => $this->status,
        ];
    }
}
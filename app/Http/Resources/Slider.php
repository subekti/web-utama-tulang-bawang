<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Slider extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
            'id'    => $this->id,
            'judul' => $this->judul,
            'link'  => $this->link,
            'img'   => $this->img,
            'img_path' => url('assets/images/slider/'.$this->instansi_id.'/'.$this->img),
        ];
		
    }
}

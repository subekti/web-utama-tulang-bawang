<?php

namespace App\Http\Resources\Frontend;

use Illuminate\Http\Resources\Json\JsonResource;

class PagesCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
		return [
            'id' => $this->id,
            'instansi_id' => $this->instansi_id,
            'judul' => $this->judul,
            'slug' => $this->slug
        ];
		
    }
}

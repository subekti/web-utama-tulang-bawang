<?php

namespace App\Http\Resources\Frontend;

use Illuminate\Http\Resources\Json\JsonResource;

class MediaCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

		return [
            'id' => $this->id,
            'type' => $this->type,
            'judul_media' => $this->judul_media,
            'judul_img' => $this->judul_img,
            'img_link' => url('/assets/images/media/').'/'.$this->img,
            'source' =>  $this->source,

        ];
		
    }
}

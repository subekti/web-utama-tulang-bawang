<?php

namespace App\Http\Resources\Frontend;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class KategoriCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
		return [
            'id' => $this->id,
            'instansi_id' => $this->instansi_id,
            'kategori' => $this->kategori,
            'slug' =>  Str::slug($this->kategori, "-"),
            'total' =>  self::find($this->id)->artikel->count(),
        ];
		
    }
}

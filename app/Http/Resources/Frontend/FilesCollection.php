<?php

namespace App\Http\Resources\Frontend;

use Illuminate\Http\Resources\Json\JsonResource;

class FilesCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         switch($this->type_file) {
        case 'pdf':
            $icon = '<i class="fa fa-file-pdf-o"></i>';
            break;
        case 'docx':
            $icon = '<i class="fa fa-file-word-o"></i>';
            break;
		default:
		    $icon = '<i class="fa fa-file-pdf-o"></i>';
            break;
		 };
		 
		return [
            'id' => $this->id,
            'judul_file' => $this->judul_file,
            'download_link' => url('/assets/file/').'/'.$this->nama_file,
            'icon' =>  $icon,
            'created_at' => $this->created_at,
            'type_file' => $this->type_file,
        ];
		
    }
}

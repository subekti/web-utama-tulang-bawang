<?php

namespace App\Http\Resources\Frontend;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class NewsCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->kategori){ $id = $this->kategori->id; $kategori = $this->kategori->kategori;}else{$id = null; $kategori = null;}
		return [
            'id' => $this->id,
            'judul_artikel' =>  Str::limit(strip_tags($this->judul_artikel), 47, " "),
            'slug' => $this->slug,
            'isi_artikel' => Str::limit(strip_tags($this->isi_artikel), 95),
            'kategori_id' => $id,
            'kategori' => $kategori,
            'parent' => $this->parent,
            'caption' => $this->caption,
            'rtanggal' => $this->tanggal,
            'tanggal' => date('d F Y', strtotime($this->tanggal)),
            'view' => $this->view,
            'link' => url('/news/read/'.$this->id.'/'.$this->slug),
			'url_img' => url('/assets/images/artikel').'/thumb_'.$this->img,
			'real_img' => url('/assets/images/artikel').'/'.$this->img,
        ];
		
    }
}

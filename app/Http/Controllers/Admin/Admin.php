<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class Admin extends Controller
{
   
    public function currentUser(){
        $access = '';
        // $serve = env('APP_URL');
        $serve = 'http://localhost:8001';
        $client = new \GuzzleHttp\Client(['exceptions' => false, 'CURLOPT_SSL_VERIFYPEER' => false]);
        $token = $_COOKIE['access_tokenku'];
        $headers = [
            	'Origin' => url('/'),        
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token,
            ];
        // $response = $client->request('GET', 'http://tulangbawangkab.go.id/api/admin/user', ['headers' => $headers]);
        // var_dump($response);

        try {
            $response = $client->request('GET', $serve.'/api/admin/user', ['headers' => $headers]); //request data dari url tersebut ke api/meta@index
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            abort(404, $e->getResponse()->getStatusCode());
        }

        $response = $response->getBody()->getContents(); //mengambil value dari $response yang berupa JSON
        $response = json_decode($response); //merubah $response menjadi array
        $access = $response->hak_akses;
        if($access == 'super admin' || $access == 'admin view'){
            $user = $response;
            $data = array(); //set variabel data untuk menampung hasil response

			foreach($user as $key=>$val){ //setiap data $instansi dijadikan menjadi $key
				if($key != 'id'); //jika key bukan bernilai string 'id' maka
				$data[$key] = $val; //atur variabel $key untuk menjadi array key pada variabel $data
            }
            
        } else { //jika user pada response->data = null maka akan tampil pesan error 404
                setcookie("access_tokenku", null);
                return abort(404, 'user not found.');
            }
            $data['user'] = $data;
            return $data;
        
        
    }
	
	
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Api as Controller;
use App\Models\Admin\Artikel as model_artikel;
use App\Models\Admin\Kategori as model_kategori;
use App\Models\Admin\Pages as model_pages;
use App\Models\Admin\Tags as model_tags;
use App\Models\Admin\Upload as model_upload;


class Home extends Controller
{
    //
	public function index(){
	
		$result['artikel'] = model_artikel::count();
		$result['kategori'] = model_kategori::count();
		$result['pages'] = model_pages::count();
		$result['tags'] = model_tags::count();
		$result['files'] = model_upload::count();
			
		return  $this->sendResponseOk($result);
	}
}

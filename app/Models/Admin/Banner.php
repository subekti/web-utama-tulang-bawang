<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
class Banner extends Model
{
    protected 	$table     = 'ads';
	protected 	$fillable  =['posisi','link','keterangan','img','status','instansi_id'];
	public 		$timestamps= false;
}

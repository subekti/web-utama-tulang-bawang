<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
	protected $fillable= ['nama_tag','tag_seo'];
	public $timestamps = false;
}
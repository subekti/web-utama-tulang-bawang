<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
class HeaderMenu extends Model
{
    protected 	$table     = 'menu';
	protected 	$fillable  =['nama_menu','id_parent','order_menu','link','status','instansi_id'];
	public 		$timestamps= false;
}
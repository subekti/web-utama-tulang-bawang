<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table   = 'kategori';
	protected $fillable= ['kategori','status'];
	public $timestamps = false;
	
	public function artikel()
    {
        return $this->hasMany('App\Models\Admin\Artikel', 'kategori_id');
    }
}

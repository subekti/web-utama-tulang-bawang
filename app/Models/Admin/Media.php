<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	protected $fillable= ['type','judul_media','judul_img','source','img','instansi_id'];
	protected $table   = 'media';
	public $timestamps = false;
}
<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable= ['slug','judul','content','keyword','description','status'];
	public $timestamps = false;
}

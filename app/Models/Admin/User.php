<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table   = 'users';
    protected $fillable= ['username','name','email','address','hak_akses','phone','password','instansi_id'];
    public $timestamps = false;
    
}

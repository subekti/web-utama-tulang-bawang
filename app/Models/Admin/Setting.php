<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $table = 'setting';
    protected $fillable= ['googlecode','judul','deskripsi','logo','alamat','telp','telp2','email','metatag','footer','fb','twitter','google','youtube','linked','metadesc','metakey','maps'];
	public $timestamps = false;
}
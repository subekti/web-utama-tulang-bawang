<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InstansiSetting extends Model
{
	protected $table = 'instansi_setting';
    protected $fillable= ['nama_kepala','kabupaten','provinsi','foto_kepala','setting_id'];
	public $timestamps = false;


}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	protected $table   = 'slider';
	protected $fillable = ['judul','link','img','instansi_id']; 
	public $timestamps = false;
}
<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
class Artikel extends Model
{
    protected 	$table     = 'artikels';
	protected 	$fillable  =['id','user_id','judul_artikel','slug','kategori_id','tag','parent','isi_artikel','metakey','metadesc','utama','tanggal','caption','img','img','instansi_id'];
	protected 	$guarded   = [];
	protected 	$attributes= ['headlines' => false, 'view' => false];
	public 		$timestamps= false;
	public function kategori()
    {
        return $this->belongsTo('App\Models\Admin\Kategori', 'kategori_id');
		 
    }
}

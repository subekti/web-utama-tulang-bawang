<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
class Headline extends Model
{
    protected 	$table     = 'artikels';
	protected 	$fillable  =['headlines'];
	public 		$timestamps= false;
	public function kategori()
    {
        return $this->belongsTo('App\Models\Admin\Kategori', 'kategori_id');
    }
}

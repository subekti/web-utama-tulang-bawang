<?php

Route::group(['prefix' => 'auth'], function () {
    //Route::post('login', 'AuthController@login')->name('login');
    Route::post('signup', 'AuthController@signup');
    Route::post('login', 'AuthController@login');
});  

Route::group(['middleware' => 'auth:api', 'admin'], function() {
    Route::get('/admin/logout', 'AuthController@logout');
	
    Route::get('/admin/user', 'AuthController@user');
    
    Route::prefix('admin/artikel/')->group(function(){
        Route::get('/list', 'Api\Artikel@index');
        Route::get('list_all', 'Api\Artikel@list_all');
        Route::get('view/{id}', 'Api\Artikel@view');
        Route::post('create/', 'Api\Artikel@create');
         Route::get('delete/{id}', 'Api\Artikel@delete');
        Route::post('update/{id}', 'Api\Artikel@update');
        Route::post('upload', 'Api\Artikel@upload');
        Route::post('unupload', 'Api\Artikel@unupload');
    });
    
    /* Route Kategori */
    Route::prefix('admin/kategori/')->group(function(){
        Route::get('list', 'Api\Kategori@index');
        Route::get('list_all', 'Api\Kategori@list_all');
        Route::get('delete/{id}', 'Api\Kategori@delete');
        Route::get('view/{id}', 'Api\Kategori@view');	
        Route::post('create/', 'Api\Kategori@create');
        Route::post('update/{id}', 'Api\Kategori@update');
    });
    
    /* Route Tags */
    Route::prefix('admin/tags/')->group(function(){
        Route::get('list', 'Api\Tags@index');
        Route::get('list_all', 'Api\Tags@list_all');
        Route::get('view/{id}', 'Api\Tags@view');
        Route::get('delete/{id}', 'Api\Tags@delete');
        Route::post('create/', 'Api\Tags@create');
        Route::post('update/{id}', 'Api\Tags@update');
    });
    
    /* Route Pages */
    Route::prefix('admin/pages/')->group(function(){
        Route::get('list', 'Api\Pages@index');
        Route::get('list_all', 'Api\Pages@list_all');
        Route::get('view/{id}', 'Api\Pages@view');
        Route::get('delete/{id}', 'Api\Pages@delete');
        Route::post('create/', 'Api\Pages@create');
        Route::post('update/{id}', 'Api\Pages@update');
		Route::post('upload', 'Api\Pages@upload');
        Route::post('unupload', 'Api\Pages@unupload');
    });
    
    /* Route Banner Ads */
    Route::prefix('admin/banner/')->group(function(){
        Route::get('list', 'Api\Banner@index');
        Route::get('view/{id}', 'Api\Banner@view');
        Route::get('delete/{id}', 'Api\Banner@delete');
        Route::post('create/', 'Api\Banner@create');
        Route::post('update/{id}', 'Api\Banner@update');
    });
    
    /* Route Headline */
    Route::prefix('admin/headline/')->group(function(){
        Route::get('list', 'Api\Headline@index');
        Route::get('list_modal', 'Api\Headline@list_modal');
        Route::get('create/', 'Api\Headline@create');
        Route::get('update/{id}', 'Api\Headline@update');
    });
    
    /* Route HeaderMenu */
    Route::prefix('admin/headerMenu/')->group(function(){
        Route::get('list', 'Api\HeaderMenu@index');
        Route::get('list_all', 'Api\HeaderMenu@list_all');
        Route::get('list_create', 'Api\HeaderMenu@list_create');
        Route::post('create/', 'Api\HeaderMenu@create');
        Route::post('update/{id}', 'Api\HeaderMenu@update');
        Route::get('view/{id}', 'Api\HeaderMenu@view');
        Route::get('delete/{id}', 'Api\HeaderMenu@delete');
    });
    
    Route::prefix('admin/upload/')->group(function(){
        Route::get('list', 'Api\Upload@index');
        Route::post('upload/', 'Api\Upload@upload');
        Route::get('open/{id}', 'Api\Upload@open');
        Route::delete('delete/{id}', 'Api\Upload@delete');
        Route::get('view/{id}', 'Api\Upload@view');
        Route::post('update/{id}', 'Api\Upload@update');
    });
    
    Route::get('admin/home/list', 'Api\Home@index');
    
    Route::prefix('admin/slider/')->group(function(){
        Route::get('list', 'Api\Slider@index');
        Route::post('create/', 'Api\Slider@create');
        Route::get('delete/{id}', 'Api\Slider@delete');
    });
    
    Route::prefix('admin/media/')->group(function(){
        Route::get('list', 'Api\Media@index');
        Route::post('upload/', 'Api\Media@upload');
        Route::get('delete/{id}', 'Api\Media@delete');
    });
    
    Route::prefix('admin/setting/')->group(function(){
        Route::get('list', 'Api\Setting@index');
        Route::post('update/{id}', 'Api\Setting@update');
        Route::post('updatemaps/{id}', 'Api\Setting@updateMap');
        Route::get('instansi_setting/', 'Api\Setting@instansiSetting');
        Route::post('update_setting_instansi/{id}', 'Api\Setting@updateInstansiSetting');
    });
    
    Route::prefix('admin/styling/')->group(function(){
        Route::get('view', 'Api\Styling@view');
        Route::post('update/{id}', 'Api\Styling@update');
    });
    
    Route::prefix('admin/user/')->group(function(){
        Route::get('list/', 'Api\User@index');
        Route::post('create/', 'Api\User@create');
        Route::post('update/{id}', 'Api\User@update');
        Route::post('check_email/{id}', 'Api\User@update');
        Route::get('view/{id}', 'Api\User@view');
        Route::delete('delete/{id}', 'Api\User@delete');
    });
});

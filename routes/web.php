<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api/meta/', 'Api\Meta@index');
Route::get('/api/meta/slider/', 'Api\Meta@slider');
Route::get('/api/news/list/', 'Api\Frontend\News@index');
Route::get('/api/news/list/pemerintahan', 'Api\Frontend\News@pemerintahan');
Route::get('/api/news/list/umum', 'Api\Frontend\News@umum');
Route::get('/api/news/kategori/{kategori}/list', 'Api\Frontend\News@kategori');
Route::get('/api/news/tag/{tag}/list', 'Api\Frontend\News@tag');
Route::get('/api/news/view/{id}/{slug}', 'Api\Frontend\News@read');
Route::get('/api/kategori/list/', 'Api\Frontend\Kategori@index');
Route::get('/api/kategori/read/{id}', 'Api\Frontend\Kategori@read');
Route::get('/api/tags/list/', 'Api\Frontend\Tags@index');
Route::get('/api/publikasi/list/', 'Api\Frontend\Files@index');
Route::get('/api/publikasi/view/{id}', 'Api\Frontend\Files@Detail');
Route::get('/api/pages/list/', 'Api\Frontend\Pages@index');
Route::get('/api/pages/view/{slug}', 'Api\Frontend\Pages@view');
Route::get('/api/gallery/list/', 'Api\Frontend\Media@index');

//web
Route::get('/', 'Frontend\Home@index');
Route::get('/home', 'Frontend\Home@home'); 
Route::get('/contact', 'Frontend\Contact@index');
Route::get('/news', 'Frontend\News@index');
Route::get('/news/read/{id}/{slug}', 'Frontend\News@read');
Route::get('/news/kategori/{id}', 'Frontend\News@kategori');
Route::get('/news/tag/{id}', 'Frontend\News@tag');
Route::get('/news/{mod}', 'Frontend\Mod@index');
Route::get('/informasi/{id}', 'Frontend\Informasi@page');
Route::get('/informasi/publikasi/', 'Frontend\Informasi@page');
Route::get('/informasi/publikasi/view/{id}', 'Frontend\Informasi@ViewPublikasi');

Route::get('/gallery/', 'Frontend\Gallery@index');

Route::get('/root', 'Frontend\Root@index');


Route::get('/logout', function () {
	//$request->cookie('access_tokenku', null);
	 
	 setcookie("access_tokenku", null);
	 
	 return redirect(url('/'));
});
Route::get('admin', 'Admin\Dashboard@index'); 

Route::get('/root', 'Frontend\Root@index');

 <!-- Header Area Start -->
<header class="header">
    <!-- Top bar start -->
	<div class="header-top bg-theme-colored sm-text-center">
      
    </div>
    <!-- Top bar start -->   
    <div class="header-top p-10 pt-20 bg-white-transparent-7 xs-text-center" /*data-bg-img="{{url('assets/images/web/bg-3.jpg')}}"*/ >
        <div class="container pt-0 pb-0">
            <div class="row">
            <div class="col-md-4">
                <a class="menuzord-brand pull-left flip xs-pull-center mt-0" href="{{url('/home')}}">
                    <img class="" src="{{$data['logo']}}" alt="Tulang Bawang" style="max-height: 75px;">
                </a>
            </div>
			
            <div class="col-md-3 hidden-xs">
                <div class="widget no-border sm-text-center mt-10 mb-10 m-0">
                <i class="fa fa-envelope text-theme-colored font-32 mt-5 mr-sm-0 sm-display-block pull-left flip sm-pull-none"></i>
                <a href="#" class="font-12 text-gray text-uppercase">Mail Us Today</a>
                <h5 class="font-12 text-black m-0">{{$data['email']}}</h5>
                </div>
            </div>
            
            <div class="col-md-3 hidden-xs">
                <!--<div class="widget no-border sm-text-center mt-10 mb-10 m-0">-->
                 <!--<i class="fa fa-building-o text-theme-colored font-32 mt-5 mr-sm-0 sm-display-block pull-left flip sm-pull-none"></i>-->
                <!--<a href="#" class="font-12 text-gray text-uppercase">Our Address</a>
               <!-- <h5 class="font-12 text-black m-0">{{$data['alamat']}}</h5>
                </div>-->
            </div>
			
			<div class="col-md-2 hidden-xs">
				<div class="widget no-border text-right flip sm-text-center mb-10 m-0">
				  <a class="btn btn-colored btn-flat btn-theme-colored mt-15 hvr-buzz-out pt-10 pb-10 font-13" data-toggle="modal" href="{{url('/')}}">Portal Tulang Bawang</a>
				</div>
			</div>
			</div>
        </div>
    </div>
    <!-- Top bar end -->

    <!-- Middle bar start -->
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-white-transparent-7">
        <div class="container bg-black-333">
            <nav id="menuzord" class="menuzord orange bg-black-333">
                <ul class="menuzord-menu">
                    <li><a href="{{url('/home')}}"><img class="mr-5 mb-5" width="18px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/home.svg" title="Selengkapnya"/> HOME</a></li>
                    <?php echo $data['menu']; ?> 
                </ul>
            </nav>
        </div>
        </div>  
    </div>

    <!-- Main navigation end -->
    </header>
        <!-- Header Area End --> 
	<script>
	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value.replace(/\+/g, ' ').replace(/\#/g, ' ');
		});
		return vars;
	}
	
	$.ajaxSetup({ timeout: 30000 });
	
	</script>			
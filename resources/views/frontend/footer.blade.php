<style>
.buybtn-panel {
    width: 400px;
}

</style>
<div class="buybtn-panel">
	<span class="buybtn-close animated-hover" id="buybtn-close"></span>
	<div class="features-panel">
		<h4 class="text-white">Agenda <span class="text-theme-colored"> Kegiatan :</span></h4>
		<ul class="fancy-list fs-normal" id="agenda">
			<div class="loader text-center"></div>
		</ul>
		<a href="{{url('news/kategori/agenda')}}" class="buybtn-cta btn btn-colored btn-flat btn-theme-colored hvr-buzz-out">Selengkapnya</a>
	</div>
	<script></script><script>
		jQuery(document).ready(function($) {
			var closeBtn = jQuery('#buybtn-close'),
				panel = closeBtn.parent();
			 
			closeBtn.on('click', function(){
				panel.toggleClass('is-hidden');
			})
		});
	</script>
</div>


<!-- Footer -->
<footer id="footer" class="bg-black-222">
      <div class="container pt-30">
        <div class="row border-bottom-black">
          <div class="col-sm-6 col-md-3">
            <div class="widget dark">
			   <h5 class="widget-title line-bottom">Pemerintah Tulang Bawang</h5>
              <p class="text-white-f1">{{$data['deskripsi']}}</p>
              
              <ul class="list-inline mt-5">
          
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="#">{{$data['email']}}</a> </li>
              </ul>
            </div>
          </div>

          <div class="col-sm-6 col-md-3">
            <div class="widget dark">
              <h5 class="widget-title line-bottom">Berita Terbaru</h5>
              <div class="latest-posts" id="recentFooter">
					<div class="loader text-center"></div>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-md-3">
              <div class="widget dark">
              <h5 class="widget-title line-bottom">Social Media</h5>
              <ul class="styled-icons icon-dark icon-bordered icon-theme-colored clearfix">
                <li><a href="{{$data['fb']}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="{{$data['twitter']}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="{{$data['linked']}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="{{$data['google']}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="{{$data['youtube']}}" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
               </ul>
            </div>
          </div>

          <div class="col-sm-6 col-md-3">
              <div class="widget dark" style="max-height: 300px;  overflow: hidden;">
                <h5 class="widget-title line-bottom">Map</h5>
                <?php echo $data['maps']; ?>
              </div>
            </div>
        </div>
      </div>
      <div class="footer-bottom bg-black-333">
        <div class="container pt-20 pb-20">
          <div class="row">
            <div class="col-md-6">
              <p class="font-12 text-gray m-0"><?php echo $data['footer'];?></p>
            </div>
            <div class="col-md-6 text-right">
              <div class="widget no-border m-0">
                <ul class="list-inline sm-text-center mt-5 font-12">
                  <li>
                    <a href="#">FAQ</a>
                  </li>
                  <li>|</li>
                  <li>
                    <a href="#">Help Desk</a>
                  </li>
                  <li>|</li>
                  <li>
                    <a href="#">Support</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  </footer>
<a class="scrollToTop" href="#" style="display: block;"><i class="fa fa-angle-up"></i></a>
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script> 
   <script type="text/javascript" src="{{url('assets/frontend')}}/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script> 
   <script src="{{url('assets/frontend')}}/js/custom.js"></script>
	
	  
<!-- Mirrored from thedesignsvilla.com/html/nirmaan/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Mar 2019 17:28:12 GMT -->
</html>
<script>
	 
	function time(){
                a = new Date();
                D1=a.getDay();
                D2=a.getDate();
                M=a.getMonth();        
                Y=a.getFullYear();
                var hari = Array("Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu");
                var bulan = Array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
               
                $("#time").html(hari[D1]+", "+D2+" "+bulan[M]+" "+Y+"");
                setTimeout("time()",800);
	}
	time();
	var image = "{{$data['preloader']}}";
	var $loading = $(".loader").html( '<img class="loading-image" src="'+image+'" alt="loading..">');
	jQuery(document).ajaxStart(function () { 
		$loading.show(); 
	});
	jQuery(document).ajaxStop(function () { 
		setTimeout('$loading.hide()',1000); 
		 
	});
	
	$(document).ajaxComplete(function(event,request,settings){
		fixImg();
		$('#gpr-kominfo-widget-footer').remove();
		//console.clear();
    });
	
	function fixImg(){
	$("img").on("error", function (err) {
	  $(this).unbind("error").attr("src", "{{url('/assets/frontend/images/404.jpg')}}");
	});
	}

  function loadAgenda(){
	var closeBtn = jQuery('#buybtn-close'),
		panel = closeBtn.parent();
	$.ajax({
			data: {"render" : "home"},
			url: BaseUrl+"/api/news/kategori/agenda/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data, function(k,v){
						content +='<li><p class="small"> Published on : '+v.tanggal+'</p><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><i class="fa fa-bell faa-ring text-theme-colored animated"></i> '+v.judul_artikel+'</a></li>';
					});

					$('#agenda').html(content);

					
				}else if(response.status == 401){
						e('info','401 server conection error');
					 
						panel.addClass('is-hidden');
				}else{
				 	
				}
			},
			dataType:'json'
		})
		setTimeout(function(){
						panel.addClass('is-hidden');
					}, 1050);
	loadRecentpostFoot();
	};
	
  loadAgenda();

  function loadRecentpostFoot(){
	$.ajax({
			data: {"render" : "footer"},
			url: BaseUrl+"/api/news/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data, function(k,v){
						content +='<article class="post media-post clearfix pb-0 mb-10">';
             
                content +='<div class="post-right">';
                    content +='<span class="post-title mt-0 mb-5"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel+'</a></span>';
                  
                content +='</div>';
                content +='</article>';
					});

					$('#recentFooter').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	loadRecentpostPub();
	};


function loadRecentpostPub(){
	$.ajax({
			data: {"render" : "footer"},
			url: BaseUrl+"/api/publikasi/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';	
					$.each(response.responseJSON.data.data, function(k,v){
						content +='<li><a href="'+BaseUrl+'/informasi/publikasi/view/'+v.id+'">'+v.judul_file+'</a></li>';
					});

					$('#recentPublication').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};

</script>
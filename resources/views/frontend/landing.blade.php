<!doctype html>
<html class="no-js" lang="en">
    
<head>
        <meta charset="utf-8">
        
        <meta http-equiv="x-ua-compatible" content="ie=edge">
       <title><?php if(isset($data['subtitle'])){ echo $data['subtitle'];}else{echo $data['title'];}; echo " | ".$data['judul'] ?></title>

        <!--=== Required Meta Tag ===-->

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metadesc']) != "")){ echo $data['metapage']['metadesc'];} else { echo $data['metadesc']; } ?>">
        <meta name="keyword" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metakey']) != "")){ echo $data['metapage']['metakey'];} else { echo $data['metakey']; } ?>">
        <meta name="author" content="Tulang Bawang">

        <meta property="og:title" content="<?php if(isset($data['subtitle'])){ echo $data['subtitle'];}else{echo $data['title'];}; echo " | ".$data['judul'] ?>" />
	    <meta property="og:description" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metadesc']) != "")){ echo $data['metapage']['metadesc'];} else { echo $data['metadesc']; } ?>" />
	    <meta property="og:type" content="website" />
	    <meta property="og:url" content="<?php echo url()->current(); ?>" />
	    <meta property="og:image" content=" <?php if(isset($data['metapage'])){ echo url('assets/images/artikel/'.$data['metapage']['img']); } elseif (isset($data['logo'])){ echo $data['logo'];} else { echo url('assets/frontend/images/logo.png');}; ?>" />
	
        <!--=== Favicon ===-->
        <link rel="shortcut icon" type="image/png" href="{{url('assets/frontend')}}/images/logo2.png" />

        <!--===+++++ Required Plugins +++++===-->
        
        <!--=== Hover ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/plugin/hover-min.css">
        <!--=== Bootstrap ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/bootstrap.min.css">
        <!--=== FontAwesome icon ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/font-awesome.min.css">
        <!--=== Linear icon ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/fonts/linear/linear.css">
        <!--=== Themify icon ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/fonts/themify/themify-icons.css">
        <!--=== Animated Headline ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/plugin/jquery.animatedheadline.css">
        <!--=== Owl carousel ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/plugin/owl.carousel.min.css">
        <!--=== Owl Theme carousel ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/plugin/owl.theme.default.min.css">
        <!--=== Main Css ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/style.css">
        <!--=== Responsive Css ===-->
        <link rel="stylesheet" href="{{url('assets/landing')}}/css/responsive.css">
		
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
        <style>
        .home {
            background-image: url(<?php echo url('assets/images/web/MPP2.jpg'); ?>);
           
        }
        .home::before {
           
                background-image: linear-gradient(to bottom right, rgb(0, 0, 0), rgba(9, 68, 105, 0.14), rgba(255, 255, 255, 0.1));
        }
        .service {
                background-color: #000352;
                background-image: linear-gradient(to top, #900600, #ff5e00, #ffd500fa);
                background-image: radial-gradient(circle, #ffd500fa, #ff5e00, #900600);
        }
        
        .service::before {
                content: " ";
                width: 100%;
                height: 100%;
                position: absolute;
                
    
        }
        .opd {
                 
                background-image: url(https://www.transparenttextures.com/patterns/beige-paper.png);
    
        }
        .service .blur-title h2 {
            opacity: 0.7;
        }
        
        .opd .blur-title h2 {
            color: white;
        }
        
        .blog {
                 
                background-image: url(https://www.transparenttextures.com/patterns/beige-paper.png);
    
        }
        
        .blog .blur-title h2 {
            color: white;
        }
        .header {
           
            background-color: #04206b7d;
           
        }
        .header .navbar-wrapper .nav-list li a span {
            color: #ffffff;
            border: 1.2px solid #ffffff;
            padding: 10px;
        }
        
        .header .navbar-wrapper .nav-list li a {
            margin-bottom: 40px;
        }
        
        .header .left-footer {
            background: #da251d61;
        }
        
        .btn-sm {
            padding: .25rem .5rem;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: .2rem;
        }
        
        @media (min-width: 1200px)
        .container {
            max-width: 80%;
        }
        </style>
		
        <script src="{{url('assets/landing')}}/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div class="main">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!--===============================================================================-->
        
        <!--====== Preloader Start ======-->
         <!-- <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>-->
        <!--====== Preloader End ======-->

        
        <!--====== Header Start ======-->
        <header class="header">
            <!-- Logo Wrapper -->
            <div class="logo-wrapper">
                <a href="#home">
                    <div class="logo"><span>TB</span></div>
                </a>
            </div>
            <!-- Navbar Wrapper -->
            <div class="navbar-wrapper">
                <ul class="nav-list">
					<li>
                        <a href="{{url('/home')}}" class="nav-list-item"><span class="lnr lnr-home"></span><span class="tooltip-data">Home</span></a>
                    </li>				
                    <li>
                        <a data-target="service" class="nav-list-item"><span class="lnr lnr-briefcase"></span><span class="tooltip-data">Service</span></a>
                    </li>
					<li>
                        <a data-target="opd" class="nav-list-item"><span class="lnr lnr-apartment"></span><span class="tooltip-data">Pemerintahan</span></a>
                    </li>
					<li>
                        <a data-target="blog" class="nav-list-item"><span class="lnr lnr-book"></span><span class="tooltip-data">Blog</span></a>
                    </li>
					<li>
                        <a data-target="testimonial" class="nav-list-item"><span class="lnr lnr-film-play"></span><span class="tooltip-data">Gallery</span></a>
                    </li>
					<li>
                        <a data-target="about" class="nav-list-item"><span class="lnr lnr-user"></span><span class="tooltip-data">About</span></a>
                    </li>
                                  
                </ul>
            </div>
            <!-- Left Footer -->
            <div class="left-footer">
                <a href="#home"><span class="lnr lnr-arrow-up"></span></a>
            </div>
        </header>
        <!--====== Header End ======-->

        <!--====== Content Area Start ======-->
        <div class="content">
            <!-- Home Start -->
            <section id="home" class="home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 hero-text">
                            <!-- Hero Text -->
                            <div class="text-center">
								<a id="hlogo" class="hvr-grow" href="{{url('/home')}}" data-toggle="tooltip" data-placement="bottom" title="Beranda Tulang Bawang" style="margin:20px;"><img width="210px" src="{{url('assets/images/web/logo.png')}}" class="rounded" alt="Tulang Bawang"></a>
                                
                                <!-- Button Wrapper -->
                                
                            </div>
                        </div>
						
						<div class="col-md-9 hero-text d-none d-sm-block">
						<div class="mt-4">
							<h1>
                                <span>KABUPATEN TULANG BAWANG</span>
                            </h1>
							<img width="220px" src="{{url('assets/landing')}}/images/aksara.png" class="rounded" alt="Tulang Bawang"><br>
							<img width="220px" src="{{url('assets/landing')}}/images/slogan.png" class="rounded" alt="Tulang Bawang">
                                
                                <div class="selector">
                                    <div class="ah-headline rotate-1">    
                                        <span class="ah-words-wrapper" style="width: 243.844px;">
                                            <b class="is-hidden">Bergerak <span class="text-lowercase">Melayani Warga</span></b>
                                            <b class="is-visible">Wellcome to tulangbawangkab.go.id</b>
                                            <b class="is-hidden">Tulang Bawang</b>
                                        </span>
                                    </div>
                                </div>
                                <!-- Button Wrapper -->
                                
                        
						</div>
						</div>
                    
                    </div>
                </div>
            </section>
            <!-- Home End -->

            <!-- Service Start -->
            <section id="service" class="service">
                <div class="container pt-50">
                    <!--<div class="row">
                        <div class="col-md-6">
                           
                            
                        </div>
                        <div class="col-md-6">
                            
                            <div class="blur-title">
                                <h2><i class="lnr lnr-briefcase"></i> Services</h2>
                            </div>
                        </div>
                    </div>--> 
                    <div class="row mt-30 mx-auto">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="LAPOR">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="https://www.lapor.go.id/instansi/pemerintah-kabupaten-tulang-bawang" target="_blank"><img src="{{ url('/assets/images/web/LAPORR.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="JDIH Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="http://jdih.tulangbawangkab.go.id" target="_blank"><img src="{{ url('/assets/images/web/JIDH.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="LPSE Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="http://lpse.tulangbawangkab.go.id" target="_blank"><img src="{{ url('/assets/images/web/LPSETB.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class=" col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="BPS Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="https://tulangbawangkab.bps.go.id/" target="_blank"><img src="{{ url('/assets/images/web/BPS.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="E-DATA Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="http://portaldata.tulangbawangkab.go.id"><img src="{{ url('/assets/images/web/E-DATA.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="LOWONGAN Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/LOWONGAN.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="PIHPS Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/PIHPS.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="INVESTASI Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/INVESTASI.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="PPID Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/PPID.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="GIS Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/GIS.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="KESEHATAN Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="#"><img src="{{ url('/assets/images/web/KESEHATAN.png') }}" alt=""></a></div>  
                            </div>
                        </div>
						
						<div class="col-6 col-sm-6 col-md-6 col-lg-3">
                            <!-- Service Block -->
                            <div class="clients-logo mb-30 hvr-bob" data-toggle="tooltip" data-placement="top" title="COVID Tulang Bawang">
                               <div class="service-block  item hvr-wobble-vertical"> <a href="http://covid19.tulangbawangkab.go.id/"><img src="{{ url('/assets/images/web/COVID.jpg') }}" alt=""></a></div>  
                            </div>
                        </div>
                   </div>
                </div>
            </section>
            <!-- Service End -->
			 <section id="opd" class="portfolio opd">
                <div class="container pt-50 pb-50">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Portfolio Title -->
                            <div class="portfolio-title title">
                                <h2 class="text-uppercase">Pemerintahan</h2>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                             <!-- Blur Title -->
                             <div class="blur-title">
                                <h2><i class="lnr lnr-apartment"></i>e-gov</h2>
                            </div>
                        </div>
                    </div>
                     <div class="row mt-30">
                        <div class="col-md-12">
                            <!-- Portfolio Menu -->
                            <div class="portfolio-menu">
                                
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".sekertariat">Sekretariat Daerah</button>
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".badan">Badan</button>
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".dinas">Dinas</button>
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".rsud">RSUD</button>
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".kecamatan">Kecamatan</button>
                                <button class="hvr-shutter-in-horizontal" type="button" data-filter=".pelayanan">MPP</button>
								<button class="hvr-shutter-in-horizontal" type="button" data-filter="all">All</button>
                            </div>
                        </div>
                    </div>
                    <!-- Portfolio Container -->
                    <div class="portfolio-container">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix xxx">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Inspektorat</h4>
										<a href="http://inspektorat.tulangbawangkab.go.id">
                                        <p class="small">Inspektorat</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix xxx">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>DPRD</h4>
										<a href="http://dprd.tulangbawangkab.go.id">
                                        <p class="small">DPRD Tulang Bawang</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix xxx">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>SATPOLPP</h4>
										<a href="http://polpp.tulangbawangkab.go.id">
                                        <p class="small">SATPOLPP</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagumum.tulangbawangkab.go.id">
                                        <p class="small">Umum</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://baghukum.tulangbawangkab.go.id">
                                        <p class="small">Hukum Perundang-undangan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagkesra.tulangbawangkab.go.id">
                                        <p class="small">Kesejahteraan Sosial</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagor.tulangbawangkab.go.id">
                                        <p class="small">Organisasi</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix pelayanan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Pelayanan</h4>
										<a href="http://mpp.tulangbawangkab.go.id">
                                        <p class="small">Mal Pelayanan Publik</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagkeu.tulangbawangkab.go.id">
                                        <p class="small">Administrasi Penatausahaan Keuangan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
						 
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagekonomi.tulangbawangkab.go.id">
                                        <p class="small">Perekonomian</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagotonomi.tulangbawangkab.go.id">
                                        <p class="small">Pemerintah dan Otonomi Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagprotokol.tulangbawangkab.go.id">
                                        <p class="small">Protokol</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bagsda.tulangbawangkab.go.id">
                                        <p class="small">SDA dan Infrastruktur</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix sekertariat">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Bagian</h4>
										<a href="http://bpbj.tulangbawangkab.go.id">
                                        <p class="small">Pengadaan Barang dan Jasa</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://bkpp.tulangbawangkab.go.id">
                                        <p class="small">Kepegawaian Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                        
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://bpbd.tulangbawangkab.go.id">
                                        <p class="small">Penganggulangan Bencana Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://bappeda.tulangbawangkab.go.id">
                                        <p class="small">Perencanaan Pembangunan Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://bapenda.tulangbawangkab.go.id">
                                        <p class="small">Pendapatan Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://bpkad.tulangbawangkab.go.id">
                                        <p class="small">Pengelolaan Keuangan & Aset Daerah</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix badan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Badan</h4>
										<a href="http://kesbangpol.tulangbawangkab.go.id">
                                        <p class="small">KESBANGPOL</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
						
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dpmptsp.tulangbawangkab.go.id">
                                        <p class="small">Penanaman Modal & Pelayanan Terpadu Satu Pintu</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://diskominfo.tulangbawangkab.go.id">
                                        <p class="small">Komunikasi & Informatika</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://disdik.tulangbawangkab.go.id">
                                        <p class="small">Pendidikan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://disparbud.tulangbawangkab.go.id">
                                        <p class="small">Kebudayaan & Pariwisata</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://perikanan.tulangbawangkab.go.id">
                                        <p class="small">Perikanan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://disdukcapil.tulangbawangkab.go.id">
                                        <p class="small">Kependudukan & Catatan Sipil</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://perdagangan.tulangbawangkab.go.id">
                                        <p class="small">Perdagangan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://pupr.tulangbawangkab.go.id">
                                        <p class="small">Pekerjaan Umum dan Penataan Ruang</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dkp.tulangbawangkab.go.id">
                                        <p class="small">Ketahanan Pangan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dlh.tulangbawangkab.go.id">
                                        <p class="small">Lingkungan Hidup</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dpmk.tulangbawangkab.go.id">
                                        <p class="small">PMK</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dishub.tulangbawangkab.go.id">
                                        <p class="small">Perhubungan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://diskepora.tulangbawangkab.go.id">
                                        <p class="small">Kepemudaan & Olahraga</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
						
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://diskop.tulangbawangkab.go.id">
                                        <p class="small">Koperasi dan UMKM</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://distani.tulangbawangkab.go.id">
                                        <p class="small">Pertanian</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
						
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://disnakertrans.tulangbawangkab.go.id">
                                        <p class="small">Tenaga Kerja & Transmigrasi</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dispusip.tulangbawangkab.go.id">
                                        <p class="small">Perpustakaan dan Arsip</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dprkp.tulangbawangkab.go.id">
                                        <p class="small">Perumahan Rakyat dan Kawasan Permukiman</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dinsos.tulangbawangkab.go.id">
                                        <p class="small">Sosial</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dinkes.tulangbawangkab.go.id">
                                        <p class="small">Kesehatan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dppkb.tulangbawangkab.go.id">
                                        <p class="small">Dinas PP,KB </p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix dinas">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Dinas</h4>
										<a href="http://dp3a.tulangbawangkab.go.id">
                                        <p class="small">DP3A</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix rsud">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>RSUD</h4>
										<a href="http://rsud.tulangbawangkab.go.id">
                                        <p class="small">RSUD Menggala</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
							<div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://banjaragung.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Banjar Agung</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://banjarbaru.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Banjar Baru</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://banjarmargo.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Banjar Margo</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                             <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://denteteladas.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Dente Teladas</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://gedungaji.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Gedung Aji</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                             <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://gedungajibaru.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Gedung Aji Baru</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://gedungmeneng.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Gedung Meneng</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                             <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://menggala.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Menggala</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://menggalatimur.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Menggala Timur</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://meraksaaji.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Meraksa Aji</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://penawaraji.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Penawar Aji</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://penawartama.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Penawar Tama</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://rawajituselatan.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Rawa Jitu Selatan</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                             <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://rawajitutimur.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Rawa Jitu Timur</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-6 col-lg-4 m-md-6 mix kecamatan">
                                <!-- Portfolio Item -->
                                <div class="portfolio-item mb-30">						
                                    <div class="portfolio-content">
                                        <h4>Kecamatan</h4>
										<a href="http://rawapitu.tulangbawangkab.go.id/" target="_blank">
                                        <p class="small">Rawa Pitu</p>
										</a>
                                    </div>
								
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Blog Start -->
            <section id="blog" class="blog pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Testimonial Title -->
                            <div class="blog-title title">
                                <h2 class="text-uppercase">KABAR <span> TERBARU </span></h2>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Blur Title -->
                            <div class="blur-title">
                                <h2><i class="lnr lnr-book"></i> News</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-30" id="recent-post">

                    </div>
                </div>
            </section>
            <!-- Blog End -->
			
			<!-- Testimonial Start -->
            <section id="testimonial" class="testimonial pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Testimonial Title -->
                            <div class="testimonial-title title">
                                <h2 class="text-uppercase">Media <span>Gallery</span></h2>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Blur Title -->
                            <div class="blur-title">
                                <h2><i class="lnr lnr-film-play"></i> Media</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-30">
                        <div class="col-md-12">
                            <!-- Carousel Start -->
                            <div class="owl-carousel testimonial-carousel owl-theme" id="media-content">
                                <!-- Carousel Item Wrapper Start -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Testimonial End -->
			 <!-- About Start -->
            <section id="about" class="about pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Portfolio Title -->
                            <div class="portfolio-title title">
                                <h2 class="text-uppercase">SEKILAS <span>TULANG BAWANG</span></h2>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="blur-title">
                                <h2><i class="lnr lnr-user"></i> About</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-30"> 
						 
						<div class="col-6 col-lg-6">
						<figure class="about-img mb-5 text-center">
						<a href="javascript:void(0)" class="kepala">
                                <img class="img-fluid img-thumbnail hvr-grow" src="http://tuba.microdataindonesia.co.id/assets/images/pages/1555581827bupati.jpg" alt="Klik untuk melihat profil">   
                        </a>
						</figure>
						</div>
						<div class="col-6 col-lg-6">
						<figure class="about-img mb-5 text-center">
						<a href="javascript:void(0)" class="kepala">
								<img class="img-fluid img-thumbnail hvr-grow" src="http://tuba.microdataindonesia.co.id/assets/images/pages/1555581853wakil.jpg" alt="Klik untuk melihat profil">  
                        </a>
						</figure>
						</div>
                        <div class="col-sm-12">
                            <!-- About Details -->
                            <div class="about-details" id="about-content">
							  
                            </div>
                            <!-- About Progress -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- About End -->
			
           
        </div>
        <!--====== Conten End ======-->


        <!--====== Footer Area Start ======-->
        <div class="footer">
            <div class="container">
                <div class="row justify-content-center">
                    <p>&copy; tulangbawangkab.go.id All Rights Reserved <span id="year"></span></p>
                </div>
            </div>
        </div>
        <!--====== Footer Area End ======-->
        </div>


        <!--==================================================================-->
        <script src="{{url('assets/landing')}}/js/jquery-3.3.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <!--=== All Plugin ===-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <!-- Popper -->
        <script src="{{url('assets/landing')}}/js/popper.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{url('assets/landing')}}/js/bootstrap.min.js"></script>
        <!-- Owl Carousel -->
        <script src="{{url('assets/landing')}}/js/plugin/owl.carousel.min.js"></script> 
        <!-- Animate Head Line -->
        <script src="{{url('assets/landing')}}/js/plugin/jquery.animatedheadline.js"></script>
        <!-- Mixit Up -->
        <script src="{{url('assets/landing')}}/js/plugin/mixitup.min.js"></script>
       
        <!--=== All Active ===-->
        <script src="{{url('assets/landing')}}/js/main.js"></script>
        <div id="modal" class="modal fade" style="z-index:1000000000" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-xl modal-notify modal-success" role="document">
			 <div class="modal-dialog modal-notify modal-success modal-xl" role="document">
			 <!--Content-->
			 <div class="modal-content">
			   <!--Header-->
			

			   <!--Body-->
			   <div class="p-1">
				 <img width="100%" src="{{url('assets/images/web/modal2.jpg')}}" class="rounded" alt="Tulang Bawang">
			   </div>

			   <!--Footer-->
			   <div class="modal-footer justify-content-center">
				 <a href="http://covid19.tulangbawangkab.go.id/" type="button" class="btn btn-sm btn-green">Pusat Informasi <i class="far fa-gem ml-1 text-white"></i></a>
				 <a type="button" class="btn btn-sm btn-outline-info waves-effect" data-dismiss="modal">Tidak, Terima Kasih</a>
			   </div>
			 </div>
			 <!--/.Content-->
		   </div>
		</div>
		</div>
		
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
		$(function () {
		  $('#modal').modal('toggle');
		  $('[data-toggle="tooltip"]').tooltip();
		  $('#hlogo').tooltip('show');
		  $("#year").text( (new Date).getFullYear() );
		})
		
		var KabUrl = "{{url('/')}}/";
		var ServeUrl = "http://services.tulangbawangkab.go.id/";
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='../../www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
			
		function loadRecentpost(){
		var dataKab = [];
		var dataOpd = [];
		$.when(
			$.ajax({
					data: {"render" : "home"},
					url: KabUrl+"api/news/list",
					method: 'GET',
					complete: function(response){ 				
						if(response.status == 200){
						dataKab = response.responseJSON.data.data.slice(0, 6);
						}else if(response.status == 401){
								e('info','401 server conection error');
						}
					},
					dataType:'json'
				}),
				
				$.ajax({
					data: {"render" : "home"},
					url: ServeUrl+"api/v1/news/skpd/list",
					method: 'GET',
					complete: function(response){ 				
						if(response.status == 200){
						dataOpd = response.responseJSON.data.data.slice(0, 6);
						}else if(response.status == 401){
								e('info','401 server conection error');
						}
					},
					dataType:'json'
				})
				
		).then(function() {
		 
				var data 	= $.merge(dataKab, dataOpd);
					data.sort(function(a,b){
					  return new Date(b.rtanggal) - new Date(a.rtanggal);
					});
					
				RenderRecentpost(data);
				
		});		
		};
		loadRecentpost();
		
		function RenderRecentpost(data){
		
			var contents = '';				
			$.each(data, function(k,v){	
			var instansi = 'Admin';
			if(v.instansi){ instansi = v.instansi.instansi; };
				contents += '<div class="col-sm-6 col-md-6 col-lg-4 m-mb-30 pb-4">';
					contents += '<div class="blog-post">';
						contents += '<figure class="thumbail-img">';
							contents += '<img src="'+v.url_img+'" alt="Post Thumbnail" class="img-fluid wow zoomIn thumbnail img-thumbnail" width="100%" style="max-height: 188.16px">';
						contents += '</figure>';
						contents += '<div class="post-details">';
							contents += '<ul class="nav small">';
								contents += '<li><a href="#">'+instansi.slice(0, 15)+'</a></li>';
								contents += '<li><a href="#">'+v.tanggal+'</a></li>';
							contents += '</ul>';
							contents += '<h4 class="mt-3"><a href="'+v.link+'">'+v.judul_artikel+'</a></h4>';
							contents += '<span class="text-white text-justify">'+v.isi_artikel.slice(0 , 80)+'...</span>';
						contents += '</div>';
					contents += '</div>';
				contents += '</div>	';								
			});
			

			$('#recent-post').append(contents);
		}
		
		function loadAbout(){
		var page = 'sejarah-tulang-bawang';
		$.ajax({
					data: "",
					url: KabUrl+"api/pages/view/"+page,
                    crossDomain: true,
                    method: 'GET',
                    complete: function(response){ 		
                        if(response.status == 200){
							var content = response.responseJSON.data.content.slice(152, 1999);
							 $("#about-content").append(content+'<br><br><a href="'+KabUrl+'informasi/sejarah-tulang-bawang" class="btn btn-md btn-green hvr-underline-from-left">Selengkapnya</a>'); 
							 
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 204){ 
							 $("#about-content").html('<center class="m-t-50 m-b-50"><h2>Oops! Not Found</h2></center>');
						}
                    },
					dataType:'json'
                })
		}
		loadAbout();
		
		function loadMedia(){
		 
		$.ajax({
					data: {"render" : "home"},
					url: KabUrl+"api/gallery/list/",
                    crossDomain: true,
                    method: 'GET',
                    complete: function(response){ 		
                        if(response.status == 200){
							var contents = '';				
							$.each(response.responseJSON.data.data, function(k,v){
							contents += '<div class="item-wrapper">';
                                    <!-- Carousel Item -->
                                   contents += '<div class="item gallery">';
									contents += '<div class="blog-post">';
										<!-- Thumbnail Image -->
										if(v.type == 'image'){
										contents += '<figure class="thumbail-img" style="max-height: 255.66px">';
											contents += '<img src="'+v.img_link+'" alt="Post Thumbnail" class="img-fluid wow zoomIn thumbnail" width="100%" style="max-height: 255.66px">';
										contents += '</figure>';
										<!-- Post Details -->
										contents += '<div class="post-details text-white">';
											 
											contents += '<h4><a data-fancybox href="'+v.img_link+'">'+v.judul_media+'</a></h4>';
										 
										contents += '</div>';
										}else if(v.type == 'embed'){
										var content = '';
										var youtube = $(v.source).html(content);
										
										contents += '<figure class="thumbail-img embed-responsive embed-responsive-16by9" style="max-height: 255.66px">';
										contents += v.source;
										contents += '</figure>';
										<!-- Post Details -->
										contents += '<div class="post-details text-white">';
											 
											contents += '<h4><a data-fancybox href="https://www.youtube.com/watch?v='+youtube.attr('src').split("/").pop()+'">'+v.judul_media+'</a></h4>';
										 
										contents += '</div>';
										}
									contents += '</div>';
                                    contents += '</div>';
                                contents += '</div>';
							});	
						
							$("#media-content").html(contents);
							$(".testimonial-carousel").owlCarousel({
								autoplay: true,
								stagePadding: 50,
								nav: false,
								loop: true,
								dots: true,
								margin: 30,
								smartSpeed:1000,
								responsiveClass:true,
								responsive:{
									0:{
										items:1,
										nav:true,
										stagePadding: 0,
										nav: false,
									},
									600:{
										items:1,
										nav:false
									},
									768:{
										items:2,
										nav:false,
										stagePadding: 0,
									},
									1000:{
										items:2,
										nav:false,
										loop:true,
									}
								}
							});
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 204){ 
							 $("#media-content").html('<center class="m-t-50 m-b-50"><h2>Oops! Not Found</h2></center>');
						}
                    },
					dataType:'json'
                })
		}
		loadMedia();
		$('#hlogo').on( 'click', function () { 
			window.location.href = KabUrl; 
		} );
		
		$('[data-fancybox]').fancybox({
			protect: true
		});
        </script>
    </body>


</html>
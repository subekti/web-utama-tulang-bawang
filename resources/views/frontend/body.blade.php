<!doctype html>
<html lang="en">
<head>
	<title><?php if(isset($data['subtitle'])){ echo $data['subtitle'];}else{echo $data['title'];}; echo " | ".$data['judul'] ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="description" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metadesc']) != "")){ echo $data['metapage']['metadesc'];} else { echo $data['metadesc']; } ?>">
	<meta name="keywords" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metakey']) != "")){ echo $data['metapage']['metakey'];} else { echo $data['metakey']; } ?>" />
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Diskominfo Tulang Bawang">

	<meta property="og:title" content="<?php if(isset($data['subtitle'])){ echo $data['subtitle'];}else{echo $data['title'];}; echo " | ".$data['judul'] ?>" />
	<meta property="og:description" content="<?php if((isset($data['metapage'])) AND (isset($data['metapage']['metadesc']) != "")){ echo $data['metapage']['metadesc'];} else { echo $data['metadesc']; } ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo url()->current(); ?>" />
	<meta property="og:image" content=" <?php if(isset($data['metapage'])){ echo url('assets/images/artikel/'.$data['metapage']['img']); } elseif (isset($data['logo'])){ echo $data['logo'];} else { echo url('assets/frontend/images/logo.png');}; ?>" />
	
	<link rel="author" href="{{url('/')}}" />
    <link rel="publisher" href="<?php echo $data['email']; ?>" />
	<link rel="shortcut icon" type="image/png" href="{{url('assets/frontend')}}/images/logo2.png" />
	
	<!-- Stylesheet -->
	<link href="{{url('assets/frontend')}}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="{{url('assets/frontend')}}/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="{{url('assets/frontend')}}/css/animate.css" rel="stylesheet" type="text/css">
	<link href="{{url('assets/frontend')}}/css/css-plugin-collections.css" rel="stylesheet"/>
	<!-- CSS | menuzord megamenu skins -->
	<link id="menuzord-menu-skins" href="{{url('assets/frontend')}}/css/menuzord-skins/menuzord-strip.css" rel="stylesheet"/>
	<!-- CSS | Main style file -->
	<link href="{{url('assets/frontend')}}/css/style-main.css" rel="stylesheet" type="text/css">

	<!-- CSS | Theme Color -->

	<link href="{{url('assets/frontend')}}/css/colors/theme-skin-{{$data['style']}}.css" rel="stylesheet" type="text/css">
	
	<!-- CSS | Preloader Styles -->
	<link href="{{url('assets/frontend')}}/css/preloader.css" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="{{url('assets/frontend')}}/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="{{url('assets/frontend')}}/css/responsive.css" rel="stylesheet" type="text/css">
	<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
	<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

	<!-- Revolution Slider 5.x CSS settings -->
	<link  href="{{url('assets/frontend')}}/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
	<link  href="{{url('assets/frontend')}}/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
	<link  href="{{url('assets/frontend')}}/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
	
	<link href="{{$data['font']}}" rel="stylesheet">
	
	<!-- external javascripts -->
	<script src="{{url('assets/frontend')}}/js/jquery-2.2.4.min.js"></script>
	<script src="{{url('assets/frontend')}}/js/jquery-ui.min.js"></script>
	<script src="{{url('assets/frontend')}}/js/bootstrap.min.js"></script>
	<!-- JS | jquery plugin collection for this theme -->
	<script src="{{url('assets/frontend')}}/js/jquery-plugin-collection.js"></script>

	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="{{url('assets/frontend')}}/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
	<script src="{{url('assets/frontend')}}/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

	
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120846200-1"></script>
	<script>
		var BaseUrl = "{{url('/')}}";
		var ServeUrl = "http://services.tulangbawangkab.go.id/";

		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-120846200-1');
		
	
	</script>
	
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c9dd3ee6f05b20011c6d7a5&product=sticky-share-buttons' async='async'></script>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c9dd3ee6f05b20011c6d7a5&product=sticky-share-buttons' async='async'></script>
</head>

<body class="{{$data['theme']}}" style="background-image: url('{{$data['bg']}}'); background-attachment: fixed;">
		<div id="wrapper">

@include('frontend.navbar')

@yield('content') 

@include('frontend.footer') 

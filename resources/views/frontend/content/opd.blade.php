@extends('frontend.body')
@section('content')
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-theme-color-3" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">{{$data['title']}}</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active text-theme-colored">{{$data['title']}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="divider  bg-white">
        <div class="container mt-30 mb-30 pt-30 pb-30">
            <div class="row">
            <div class="col-md-9">
				<div id="news-list">
						
				</div>
             <div class="text-center"><div class="loader text-center"></div><a id="loadmore" class="btn btn-colored btn-flat btn-theme-colored hvr-overline-from-center mt-15 pr-40 pl-40" onclick="loadMore()" data-value=""><strong><i class="fa fa-circle-o-notch"></i> Load More</strong></a></div>   
            </div>
            <div class="col-md-3">
                    <div class="sidebar sidebar-right mt-sm-30">
						<div class="widget">
						  <h4 class="widget-title line-bottom">
								<span>SEARCH <span class="text-theme-colored">BOX</span></span> 
								<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/fine_print.svg" title="Selengkapnya"/></span>
							</h4>
						  <div class="search-form">
							<form action="{{url('/news/opd')}}">
							  <div class="input-group">
								<input name="keyword" type="text" placeholder="Click to Search" class="form-control search-input">
								<span class="input-group-btn">
								<button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
								</span>
							  </div>
							</form>
						  </div>
						</div>
						<div class="widget">
						  <h4 class="widget-title line-bottom">
								<span>INFO <span class="text-theme-colored">PENGUMUMAN</span></span> 
								<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/pengumuman')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"></a></span>
							</h4>
						  <ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#pengumuman-kab" aria-expanded="true">KABUPATEN</a></li>
							<li class=""><a data-toggle="tab" href="#pengumuman-skpd" id="btn-pengumuman-skpd"aria-expanded="false">SKPD</a></li>
							 
						  </ul>
						  <div class="tab-content">
							<div id="pengumuman-kab" class="tab-pane fade active in">
							  <div class="loader text-center"></div>
							</div>
							<div id="pengumuman-skpd" class="tab-pane fade">
							  <div class="loader text-center"></div>
							</div>
							 
						  </div>
						</div>
					  <div class="widget">
					  	<h4 class="widget-title line-bottom">
								<span>TRENDING <span class="text-theme-colored">POSTS</span></span> 
								<span class="pull-right"><a class="text-theme-colored" href="{{url('news?type=popular')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/positive_dynamic.svg" title="Selengkapnya"/></a></span>
							</h4>
						  <ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#recent-opd" aria-expanded="true">OPD</a></li>
							<li class=""><a data-toggle="tab" href="#recent-kecamatan" id="btn-recent-kecamatan" aria-expanded="false">KECAMATAN</a></li>
							 
						  </ul>
						  <div class="tab-content">
							<div id="recent-opd" class="tab-pane fade active in">
							  <div class="loader text-center"></div>
							</div>
							<div id="recent-kecamatan" class="tab-pane fade">
							  <div class="loader text-center"></div>
							</div>
							 
						  </div>
						</div>
                       <div class="widget">
					   	<h4 class="widget-title line-bottom">
								<span>GPR <span class="text-theme-colored">ARTIKEL</span></span> 
								<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/kindle.svg" title="Selengkapnya"/></span>
							</h4>
                        <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script> 
						<div id="gpr-kominfo-widget-container"></div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
        <!-- Blog page section end -->
</div>
	<script>
	function slugify(string) {
	  if(string){
	  return string
		.toString()
		.trim()
		.toLowerCase()
		.replace(/\s+/g, "-")
		.replace(/[^\w\-]+/g, "")
		.replace(/\-\-+/g, "-")
		.replace(/^-+/, "")
		.replace(/-+$/, "");
	  }
	}
	
	var page 	= {"page" : 1};
	var extend  = getUrlVars();
	var data 	= $.extend(extend, page);
	function loadNews(data){
		if(getUrlVars().keyword){ $('input[name=keyword]').val(getUrlVars().keyword); }
		var uri = window.location.pathname.split('/');
		var opt = "news";
		console.log($.inArray('kategori', uri));
		if($.inArray('kategori', uri) >= 1){
			opt = 'news/kategori/'+window.location.pathname.split('/').pop();
		}else if($.inArray('tag', uri) >= 1){
			opt = 'news/tag/'+window.location.pathname.split('/').pop();
		}
		$.ajax({
					data: data,
					url: ServeUrl+"api/v1/"+opt+"/skpd/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = ''; 
							$.each(response.responseJSON.data.data, function(k,v){
								 
								content +='<div class="upcoming-events bg-warning mb-20">';
									content +='<div class="row">';
										content +='<div class="col-sm-4 pr-0 pr-sm-15">';
										content +='<div class="thumb p-15">';
											content +='<img class="img-fullwidth thumbnail" src="'+v.url_img+'" alt="'+v.judul_artikel+'">';
										content +='</div>';
										content +='</div>';
										content +='<div class="col-sm-5 pl-0 pl-sm-15">';
										content +='<div class="event-details p-5 mt-20">';
											content +='<h4 class="media-heading text-uppercase font-weight-500">'+v.judul_artikel+'</h4>';
											content +='<p class="text-justify">'+v.isi_artikel.substring(0, 90)+'..</p>';
											content +='<a href="'+v.link+'" class="btn btn-flat btn-dark btn-theme-colored hvr-buzz-out" target="blank">Details <i class="fa fa-angle-double-right"></i></a>';
										content +='</div>';
										content +='</div>';
										content +='<div class="col-sm-3">';
										content +='<div class="event-count p-5 mt-15">';
											content +='<ul class="event-date list-inline font-14 mt-10 mb-20">';
											content +='<li class="p-5 pl-10 pr-10 mr-5 bg-lighter">'+v.tanggal.split(' ')[1]+'</li>';
											content +='<li class="p-5 pl-15 pr-15 mr-5 bg-info"> '+v.tanggal.split(' ')[0]+'</li>';
											content +='<li class="p-5 bg-primary">'+v.tanggal.split(' ')[2]+'</li>';
											content +='</ul>';
											content +='<ul>';
											content +='<li class="small"><a href="'+v.instansi.oorigin+'">'+v.instansi.instansi+'</a></li>';
											content +='<li class="mb-10 text-theme-colored small"><i class="fa fa-tag mr-5"></i> <a target="blank" class="hvr-grow" href="'+v.instansi.origin+'/news/kategori/'+slugify(v.kategori)+'">'+v.kategori+'</a></li>';
											content +='<li class="text-theme-colored small"><i class="fa fa-heart-o mr-5"></i> '+v.view+' Views</li>';
											content +='</ul>';
										content +='</div>';
										content +='</div>';
									content +='</div>';
								content +='</div>';
								
							});
							$('#loadmore').data("value", response.responseJSON.data.current_page);
							$('#news-list').append(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 204){
							 $('#loadmore').remove();
							 $('#news-list').append('<center class="m-t-50"><h4>Oops! Not Found</h4></center>');
						}
                    },
					dataType:'json'
        })
	
	};
	loadNews(data);
	
	function loadPengumuman(){
	$.ajax({
			data: {"render" : "sidebar"},
			url: BaseUrl+"/api/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){

							  content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right">';
									content += '<h5 class="post-title mt-0"><a class="hvr-grow" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><i class="fa fa-bullhorn mr-5 text-theme-colored faa-flash animated"></i> '+v.judul_artikel.substring(0,25)+'..</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							  content += '</article>';
					});

					$('#pengumuman-kab').html(content);
				
				}else if(response.status == 401){
						e('info','401 server conection error');
				}else{
				 	$("#btn-pengumuman-skpd").trigger("click");
				}
			},
			dataType:'json'
		})
	 
	};
	loadPengumuman();
	
	$('#btn-pengumuman-skpd').click(function(){
		
		$.ajax({
			data: "",
			url: ServeUrl+"/api/v1/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a target="_blank" href="'+v.link+'">'+v.judul_artikel.toUpperCase()+'</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i>'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							content += '</article>';
					});
					
					
					$('#pengumuman-skpd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		}) 
	});
	
	function loadOpd(){
	$.ajax({
			data: {"render" : "sidebar", "type" : "popular"},
			url: ServeUrl+"api/v1/news/skpd/list",
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 10), function(k,v){
							content += '<article class="post media-post clearfix pb-1 mb-10">';
								content += '<div class="post-right">';
									content += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel+'</a></h5>';
									content += '<p class="post-date mb-10 font-11 font-weight-600">'+v.instansi.instansi.substring(0,31)+'<br> <i class="fa fa-calendar mr-5 text-theme-colored"></i> '+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					content += '</div>';
							content += '</article>';
					});

					$('#recent-opd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};
	loadOpd();
	
	$('#btn-recent-kecamatan').click(function(){
    	$.ajax({
			data: {"render" : "sidebar", "type" : "popular"},
			url: ServeUrl+"api/v1/news/kecamatan/list",
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 10), function(k,v){
							content += '<article class="post media-post clearfix pb-1 mb-10">';
								content += '<div class="post-right">';
									content += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel+'</a></h5>';
									content += '<p class="post-date mb-10 font-11 font-weight-600">'+v.instansi.instansi.substring(0,31)+'<br> <i class="fa fa-calendar mr-5 text-theme-colored"></i> '+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					content += '</div>';
							content += '</article>';
					});

					$('#recent-kecamatan').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})	
	});
	function loadMore(){
		var page = parseInt($('#loadmore').data("value"))+1;
		var page = {"page" : page};
		var extend  = getUrlVars();
		var data 	= $.extend(extend, page);
		loadNews(data);		
	};
	
	
	</script>    
@stop
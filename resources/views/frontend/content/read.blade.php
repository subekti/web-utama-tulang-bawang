@extends('frontend.body')
@section('content')
<div class="main-content">	
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">{{$data['title']}}</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/news')}}">News</a></li>
                <li class="active text-theme-colored">{{$data['subtitle']}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
	
    <section class="devider  bg-white">
        <div class="container">
            <div class="row">
              <div class="col-md-9">
				<div class="blog-posts single-post">
				  <article class="post clearfix mb-0">
					
					<div class="entry-content">
					  <div class="entry-meta media no-bg no-border mt-5 pb-20">
						<div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
						  <ul>
							<li class="font-16 text-white font-weight-600"><?php echo explode(" ", $data['metapage']['tanggal'])[0]; ?></li>
							<li class="font-12 text-white text-uppercase"><?php echo explode(" ", $data['metapage']['tanggal'])[1]; ?></li>
						  </ul>
						</div>
						<div class="media-body pl-15">
						  <div class="event-content pull-left flip">
							<h3 class="entry-title text-white pt-0 mt-0"><a href="blog-single-right-sidebar.html"><?php echo $data['metapage']['judul_artikel']; ?></a></h3>
							<span class="mb-10 mr-10 font-13"><i class="fa fa fa-calendar mr-5 text-theme-colored"></i> <?php echo $data['metapage']['tanggal']; ?></span>                       
							<span class="mb-10 mr-10 font-13"><i class="fa fa-tag mr-5 text-theme-colored"></i> <a href="<?php echo url('news/kategori/'.Str::slug(strtolower($data['metapage']['kategori']['kategori']))); ?>"><?php echo $data['metapage']['kategori']['kategori']; ?></a></span>                       
							<span class="mb-10 mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> <?php echo $data['metapage']['view']; ?> Views</span>
						  </div>
						</div>
					  </div>
					 
					
					</div>
					<?php if(($data['metapage']['kategori']['kategori'] != 'Agenda') AND ($data['metapage']['kategori']['kategori'] != 'Pengumuman')){ ?>
					<div class="entry-header">
					  <div class="post-thumb thumb"> 
						<img src="<?php echo url('assets/images/artikel/'.$data['metapage']['img']); ?>" alt="" class="img-responsive img-fullwidth">
						 
					  </div>
					</div>
					<?php } ?>
					<div class="entry-content">
						<p class="mb-20 text-center small"><?php echo $data['metapage']['caption']; ?></p>
						<?php echo $data['metapage']['isi_artikel']; ?>
					</div>
					<hr>
					
					
				  </article>
				  <div class="tagline p-0 pt-20 mt-5">
					<div class="row">
					  <div class="col-md-8">
						<div class="tags">
						  <p class="mb-0" id="tagsme"><i class="fa fa-tags text-theme-colored"></i> <span>Tags:</span> 
						  <?php $tags = explode(",", $data['metapage']['tag']); foreach($tags as $tag){ ?>
							<a href="<?php echo url('/news/tag/'.strtolower($tag)); ?>"># <?php echo $tag; ?>, </a>
						  <?php } ?>
						  </p>
						</div>
					  </div>
					   
					</div>
				  </div>
				  <div class="comments-area">
				      <div class="sharethis-inline-reaction-buttons"></div>
					<h5 class="comments-title">Comments</h5>
					<div id="fb-root"></div>
                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>
                    <div class="fb-comments" data-href="{{ url()->current() }}" data-width="100%" data-numposts="5"></div>
				  </div>
				  
				</div>
			  </div>
          
            <div class="col-md-3">
                    <div class="sidebar sidebar-right mt-sm-30">
						<div class="widget">
						  <h5 class="widget-title line-bottom">SEARCH BOX</h5>
						  <div class="search-form">
							<form action="{{url('/news')}}">
							  <div class="input-group">
								<input name="keyword" type="text" placeholder="Click to Search" class="form-control search-input">
								<span class="input-group-btn">
								<button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
								</span>
							  </div>
							</form>
						  </div>
						</div>
                      <div class="widget">
                        <h5 class="widget-title line-bottom">KATEGORI</h5>
                        <ul class="list-group" id="category">
						  <div class="loader text-center"></div>
                          
                        </ul>
                      </div>
                      <div class="widget">
                        <h5 class="widget-title line-bottom">TRENDING POSTS</h5>
                        <div class="widget-image-carousel" id="popular">
                        
						</div>
                      </div>
					  <div class="widget">
                        <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script> 
						<div id="gpr-kominfo-widget-container"></div>
                      </div>
                      <div class="widget">
                        <h5 class="widget-title line-bottom">TAGS</h5>
                        <div class="tags" id="tags">
                          
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
        <!-- Blog page section end -->
</div>
	<script>
	function slugify(string) {
	  if(string){
	  return string
		.toString()
		.trim()
		.toLowerCase()
		.replace(/\s+/g, "-")
		.replace(/[^\w\-]+/g, "")
		.replace(/\-\-+/g, "-")
		.replace(/^-+/, "")
		.replace(/-+$/, "");
	  }
	}
	
	function loadSidebar(){

		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/kategori/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
							
								content +=' <li class="list-group-item post-title"><a href="'+BaseUrl+'/news/kategori/'+v.slug+'">'+v.kategori+'</a> <span class="badge">'+v.total+'</span></li>';

							});
							
							$('#category').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
                });
				
		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/tags/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
								content +='<a class="hvr-buzz-out" href="'+BaseUrl+'/news/tag/'+v.tag_seo+'">'+v.nama_tag+'</a>';
							});
							
							$('#tags').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        })
	loadPopular();
	};
	loadSidebar();
	
	function loadPopular(){
		$.ajax({
					data: {"render" : "sidebar", "type" : "popular"},
					url: BaseUrl+"/api/news/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){

								content += '<div class="latest-posts">';
									content += '<article class="post media-post clearfix pb-0 mb-5">';
									content += '<a class="post-thumb" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><div class="img-sidebar"><img class="wow fadeIn" data-wow-duration="3s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
										content += '<div class="post-right">';
										content += '<h5 class="post-title mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel+'</a></h5>';
										content += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-calendar mr-5 text-theme-colored"></i> '+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
										content += '</div>';
									content += '</article>';
								content += '</div>';
							});
							
							$('#popular').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
                })
	
	};
	
	
	function loadMore(){
		var page = parseInt($('#loadmore').data("value"))+1;
		var page = {"page" : page};
		var extend  = getUrlVars();
		var data 	= $.extend(extend, page);
		loadNews(data);		
	};
	</script>    
@stop
@extends('frontend.body')
@section('content')
<div class="main-content">
         <!-- Page title section start -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">{{$data['title']}}</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active text-theme-colored">{{$data['title']}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
        <!-- Page title section end -->
        <!-- Blog page section start -->
        <section class="divider bg-white p-t-70 p-b-70">
            <div class="container">
                <div class="row">
					<div class="col-md-3 section-white p-t-20">
                        <div class="sidebar sidebar-right mt-sm-30">
                            
					<div class="widget">
						<h4 class="widget-title line-bottom">
							<span>INFO <span class="text-theme-colored">PENGUMUMAN</span></span> 
							<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/pengumuman')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"></a></span>
						</h4>
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#pengumuman-kab" aria-expanded="true">KABUPATEN</a></li>
							<li class=""><a data-toggle="tab" href="#pengumuman-skpd" id="btn-pengumuman-skpd"aria-expanded="false">SKPD</a></li>
						</ul>
						<div class="tab-content">
							<div id="pengumuman-kab" class="tab-pane fade active in">
								<div class="loader text-center"></div>
							</div>
							<div id="pengumuman-skpd" class="tab-pane fade">
								<div class="loader text-center"></div>
							</div>
						</div>
					</div> 
					
					<div class="widget">
						<h4 class="widget-title line-bottom">
							<span>INFO <span class="text-theme-colored">PUBLIKASI</span></span> 
							<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/publikasi')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/opened_folder.svg" title="Selengkapnya"></a></span>
						</h4>
						<ul id="publikasi" class="list-group">
								<div class="loader text-center"></div>
						</ul>
					</div>
					
					<div class="widget">
						<h4 class="widget-title line-bottom">
							<span>PUBLIKASI <span class="text-theme-colored">OPD</span></span> 
							<span class="pull-right"><a class="text-theme-colored" href="#"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/opened_folder.svg" title="Selengkapnya"></a></span>
						</h4>
						<ul id="publikasi-opd" class="list-group">
								<div class="loader text-center"></div>
						</ul>
					</div>
 
                        </div>
                    </div>
                    <div class="col-md-9">
					<div class="border-1px p-20 pr-10">
					<div id="deskripsi" class="mb-20"></div>
					<p>Berikut adalah informasi data file yang dapan anda unduh :</p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                            <td>Judul File</td>
                            <td id="judul"></td>
                            </tr>
                            <tr>
                            <td>Nama File </td>
                            <td id="file"></td>
                            </tr>
                            <tr>
                            <td>Type File</td>
                            <td id="type"></td>
                            </tr>
                            <tr>
                            <td>Tanggal Upload</td>
                            <td id="date"></td>
                            </tr>
                            <tr>
                            <td>Download </td>
                            <td class="read-more" id="download">
                            </td>
                            </tr>
                        </tbody>
                        </table>
                   </div>
                   </div>
				</div>
            </div>
        </section>
</div>
       <!-- Blog page section end -->
	<script>
	function loadView(){
		var page = window.location.pathname.split('/').pop();
		$.ajax({
					data: "",
					url: BaseUrl+"/api/publikasi/view/"+page,
                    crossDomain: true,
                    method: 'GET',
                    complete: function(response){ 		
                        if(response.status == 200){
							
							 $(".title").html(response.responseJSON.data.judul_file);
							 $("#judul").html(response.responseJSON.data.judul_file);
							 $("#deskripsi").html(response.responseJSON.data.deskripsi_file);
							 $("#file").html('<b>'+response.responseJSON.data.nama_file+'</b> <i>(encrypted)</i>');
							 $("#type").html(response.responseJSON.data.type_file);
							 $("#date").html(response.responseJSON.data.created_at);
							 $("#download").html('<a class="btn btn-colored btn-flat btn-theme-colored" href="'+response.responseJSON.data.link_download+'"><span class="glyphicon glyphicon-save"></span> Download</a>');
							 
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 204){ 
							 $("#blog-content").html('<center class="m-t-50 m-b-50"><h2>Oops! Not Found</h2></center>');
						}
                    },
					dataType:'json'
                })
                loadSidebar();
	}
	
	loadView();
	
	function loadPengumuman(){
	$.ajax({
			data: {"render" : "sidebar"},
			url: BaseUrl+"/api/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){

							  content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"> '+v.judul_artikel.toUpperCase()+'..</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							  content += '</article>';
					});

					$('#pengumuman-kab').html(content);
				
				}else if(response.status == 401){
						e('info','401 server conection error');
				}else{
				 	$("#btn-pengumuman-skpd").trigger("click");
				}
			},
			dataType:'json'
		})
	 
	};
	loadPengumuman();
	function loadSidebar(){		
		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/publikasi/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
						content +=' <li class="list-group-item hvr-shadow"><div class="pull-left"><i class="fa fa-download mr-10 mb-50 text-theme-colored faa-float animated float-left"></i></div><h5 class="post-title"><a class="" href="'+BaseUrl+'/informasi/publikasi/view/'+v.id+'"> '+v.judul_file+'</a></h5> <p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.created_at+'</span></p></li>';
							
							});
							
							$('#publikasi').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        });
		$.ajax({
					data: {"render" : "sidebar"},
					url: ServeUrl+"api/v1/publikasi/skpd/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
							content +=' <li class="list-group-item hvr-shadow"><h5 class="post-title"><a target="_blank" href="'+v.link+'"><i class="fa fa-download mr-5 text-primary faa-float animated"></i> '+v.judul_file.substring(0,25).toUpperCase()+'</a></h5> <p class="post-date ml-20 font-11 font-weight-600">'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.date+'</span></p></li>';
							});
							
							$('#publikasi-opd').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        })
	
	};
	loadView();
	
	$('#btn-pengumuman-skpd').click(function(){
		
		$.ajax({
			data: "",
			url: ServeUrl+"/api/v1/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a target="_blank" href="'+v.link+'">'+v.judul_artikel.toUpperCase()+'</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i>'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							content += '</article>';
					});
					
					
					$('#pengumuman-skpd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		}) 
	});

</script>	
@stop
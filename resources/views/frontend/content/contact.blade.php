@extends('frontend.body')
@section('content')
<div class="main-content">
         <!-- Page title section start -->
         <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="assets/images/web/Bupati-dan-Wakil-Bupati-Tulang-Bawang.jpg">
            <div class="container pt-60 pb-60">
              <!-- Section Content -->
              <div class="section-content">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <h2 class="title">{{$data['title']}}</h2>
                    <ol class="breadcrumb text-center text-black mt-10">
                      <li><a href="{{url('/')}}">Home</a></li>
                      <li class="active text-theme-colored">{{$data['title']}}</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </section>
        <!-- Page title section end -->
        <!-- Section: Have Any Question -->
        <section data-bg-img="{{url('assets/images/web/bg-5.jpg')}}">
            <div class="container pt-60 pb-60">
              <div class="section-title mb-60">
                <div class="row">
                  <div class="col-md-12">
                    <div class="esc-heading small-border text-center">
                      <h3>Have any Questions?</h3>
                    </div>
                  </div>
                </div>
              </div>
              <div class="section-content">
                <div class="row">
                  <div class="col-sm-12 col-md-4">
					<div class="icon-box left media bg-white p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
					  <div class="media-body">
						<h5 class="mt-0">Address Office</h5>
						<p>{{$data['alamat']}}</p>
					  </div>
					</div>
                  </div>
                  <div class="col-sm-12 col-md-4">
                    <div class="icon-box left media bg-warning p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-call text-theme-colored"></i></a>
					  <div class="media-body">
						<h5 class="mt-0">Call Us</h5>
						<p>{{$data['telp']}} | {{$data['telp2']}}<br><br></p>
					  </div>
					</div>
                  </div>
                  <div class="col-sm-12 col-md-4">
					<div class="icon-box left media bg-info p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-colored"></i></a>
					  <div class="media-body">
						<h5 class="mt-0">Email Address</h5>
						<p>{{$data['email']}}<br><br></p>
					  </div>
					</div>
                     
                  </div>
                </div>
              </div>
            </div>
          </section>
		  
		<section class="clients divider parallax layer-overlay overlay-dark-4" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
		  <div class="container pt-40 pb-10">
			 
			<div class="section-content text-center">
			<div class="row">
			  <div class="col-md-12 text-center">
				<!-- Section: Clients -->
				 
				<div class="owl-carousel-6col clients-logo text-center " data-nav="false">
				  <div class="item hvr-wobble-vertical"> <a href="http://lpse.tulangbawangkab.go.id"><img src="{{url('assets/images/web/LPSETB.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="http://jdih.tulangbawangkab.go.id"><img src="{{url('assets/images/web/JIDH.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="http://lapor.go.id"><img src="{{url('assets/images/web/LAPORR.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="https://tulangbawangkab.bps.go.id/"><img src="{{url('assets/images/web/BPS.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#"><img src="{{url('assets/images/web/PIHPS.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#"><img src="{{url('assets/images/web/INVESTASI.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#"><img src="{{url('assets/images/web/E-DATA.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#"><img src="{{url('assets/images/web/LOWONGAN.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#"><img src="{{url('assets/images/web/KESEHATAN.png')}}" alt=""></a></div>
				</div>
			  </div>
			</div>
			</div>
		  </div>
		</section>
        <!-- Divider: Google Map -->
        <section class="divider  bg-white">
            <div class="container-fluid pt-0 pb-0">
              <div class="row">
                <?php echo $data['maps'] ?>
                <script src="js/google-map-init-multilocation.js"></script>
              </div>
            </div>
        </section>
         <!-- Divider: Clients -->
</div>		 
		

@stop
@extends('frontend.body')
@section('content')
<style>
.thumbnail { margin-bottom: 0px; 
</style>
<div class="main-content">
		<section id="home">
		<div class="container-fluid p-0">
		<div class="rev_slider_wrapper">
				<div class="rev_slider" data-version="5.0">
					<ul>
					<!-- SLIDE 1 -->
					<?php $n = 0; foreach($data['slider'] as $item){ ?>

					<li data-index="rs-<?php echo $n++; ?>" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ $item['img'] }}" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
						<!-- MAIN IMAGE -->
					<img src="{{ $item['img'] }}"  alt=""  data-bgposition="center 40%" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						
		
 
		
						
					</li>
					<?php } ?>					
					</ul>
				</div><!-- end .rev_slider -->
		</div>
		</div>
		</section>

		<section class="bg-white">
		<div class="container">
			<div class="row">

				<div class="col-md-9">
					<div class="row mb-20">
							<div class="col-md-12">
								<h3 class="title-pattern mb-30">
									<span>BERITA <span class="text-theme-colored">TERBARU</span></span>
									<span class="pull-right pl-5"><a class="text-theme-colored" href="{{url('news/')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/parallel_tasks.svg" title="Selengkapnya"/></a></span>
								</h3>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="recent-left">
								<div class="loader text-center"></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="recent-right">
								<div class="loader text-center"></div>
								</div>
							</div>
					</div>
					
					 
					
					<div class="row mb-20">
							<div class="col-md-12">                  
							    <h3 class="title-pattern mb-30">
									<span>BERITA <span class="text-theme-colored">SKPD</span></span>
									<span class="pull-right pl-5"><a class="text-theme-colored" href="{{url('news/opd')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/parallel_tasks.svg" title="Selengkapnya"/></a></span>
								</h3>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="opd-left">
								<div class="loader text-center"></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="opd-right">
								<div class="loader text-center"></div>
								</div>
							</div>
					</div>

					<div class="row mb-20">
							<div class="col-md-12">                  
								<h3 class="title-pattern mb-30">
									<span>BERITA <span class="text-theme-colored">UMUM</span></span>
									<span class="pull-right pl-5"><a class="text-theme-colored" href="{{url('news/kategori/umum')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/parallel_tasks.svg" title="Selengkapnya"/></a></span>
								</h3>
							</div>
							<div id="umum">
								<div class="loader text-center"></div>
							</div>
					</div>
					
					<div class="row mb-20">
							<div class="col-md-12">                  
							    <h3 class="title-pattern mb-30">
									<span>BERITA <span class="text-theme-colored">KECAMATAN</span></span>
									<span class="pull-right pl-5"><a class="text-theme-colored" href="{{url('news/kecamatan')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/parallel_tasks.svg" title="Selengkapnya"/></a></span>
								</h3>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="kecamatan-left">
								<div class="loader text-center"></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div id="kecamatan-right">
								<div class="loader text-center"></div>
								</div>
							</div>
					</div>
					
				</div>
				
				<div class="col-md-3 col-sm-12 ">
				 
						<div class="widget text-center">
								<!--<h4 class="widget-title pt-10">
									<span class="text-theme-colored">KEPALA DAERAH</span>
								</h4> -->
							<a class="hvr-grow" href="{{url('informasi/profile-kepala-daerah')}}"><img class="img-fullwidth thumbnail" src="{{$data['foto_kepala']}}" alt=""></a>
							<h6>{{$data['nama_kepala']}}</h6>
						</div>
						 
						<div class="widget">
						  <h4 class="widget-title line-bottom">
								<span>INFO <span class="text-theme-colored">PENGUMUMAN</span></span> 
								<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/pengumuman')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"/></a></span>
							</h4>
						  <ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#pengumuman-kab" aria-expanded="true">KABUPATEN</a></li>
							<li class=""><a data-toggle="tab" href="#pengumuman-skpd" id="btn-pengumuman-skpd"aria-expanded="false">SKPD</a></li>
							 
						  </ul>
						  <div class="tab-content" style="background-color: #f5dcdc85;">
							<div id="pengumuman-kab" class="tab-pane fade active in">
							  <div class="loader text-center"></div>
							</div>
							<div id="pengumuman-skpd" class="tab-pane fade">
							  <div class="loader text-center"></div>
							</div>
							 
						  </div>
						</div>
						<div class="widget">
							<h4 class="widget-title line-bottom">
								<span>INFO <span class="text-theme-colored">PUBLIKASI</span></span> 
								<span class="pull-right"><a class="text-theme-colored" href="{{url('informasi/publikasi')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/opened_folder.svg" title="Selengkapnya"/></a></span>
							</h4>
							<ul id="publikasi" class="list-group">
									<div class="loader text-center"></div>
							</ul>
						</div>
						<!--<div class="widget">
					  	<h4 class="widget-title line-bottom">
								<span>TRENDING <span class="text-theme-colored">POSTS</span></span> 
								<span class="pull-right"><a class="text-theme-colored" href="{{url('news?type=popular')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/positive_dynamic.svg" title="Selengkapnya"/></a></span>
							</h4>
							<div class="latest-posts">
								<span id="popullar">
									<div class="loader text-center"></div>
								</span>
							</div>
						</div>-->
						
						<?php if(isset($data['banner']['sidebar'])){   ?>
						<div class="widget mb-20">
						    <h4 class="widget-title line-bottom">
									<span>INFO <span class="text-theme-colored">SOSIALISASI</span></span>
									<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/frame.svg" title="Selengkapnya"/></span> 					
							</h4>
							<?php if($data['banner']['sidebar']['link'] != ''){ ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar']['link']; ?>">
							<?php } else { ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar']['img']; ?>" data-lightbox-gallery="gallery" title="<?php echo $data['banner']['sidebar']['keterangan']; ?>">
							<?php } ?>
							<img class="img-fullwidth thumbnail" src="<?php echo $data['banner']['sidebar']['img']; ?>" alt="">
							</a>
						 </div>
						 <?php } ?>
						 <?php if(isset($data['banner']['sidebar2'])){   ?>
						<div class="widget">
							<?php if($data['banner']['sidebar2']['link'] != ''){ ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar2']['link']; ?>">
							<?php } else { ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar2']['img']; ?>" data-lightbox-gallery="gallery" title="<?php echo $data['banner']['sidebar2']['keterangan']; ?>">
							<?php } ?>
							<img class="img-fullwidth thumbnail" src="<?php echo $data['banner']['sidebar2']['img']; ?>" alt="">
							</a>
						 </div>
						 <?php } ?>
						  
						  
						  <div class="widget">
						  <h4 class="widget-title line-bottom">
								<span>INFO <span class="text-theme-colored">GEMPA</span></span> 
								<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"/></span>
							</h4>
						  <ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#gempa-1" aria-expanded="true">M ≥ 5.0 SR</a></li>
							<li class=""><a data-toggle="tab" href="#gempa-2" id="btn-gempa-2" aria-expanded="false">Dirasakan</a></li>
							 
						  </ul>
						  <div class="tab-content p-5">
							<div id="gempa-1" class="tab-pane fade active in">
							  <div class="loader text-center"></div>
							</div>
							<div id="gempa-2" class="tab-pane fade">
							  <div class="loader text-center"></div>
							</div>
							 
						  </div>
						</div>
							
					</div>	
			
			</div>

		</div>
		</section>
		
		 <!-- Divider: Clients -->
		<section class="clients divider parallax layer-overlay overlay-dark-4" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
		  <div class="container pt-40 pb-10">
			 
			<div class="section-content text-center">
			<div class="row">
			  <div class="col-md-12 text-center">
				<!-- Section: Clients -->
				 
				<div class="owl-carousel-6col clients-logo text-center " data-nav="false">
				  <div class="item hvr-wobble-vertical"> <a href="http://lpse.tulangbawangkab.go.id" target="_blank"><img src="{{url('assets/images/web/LPSETB.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="http://jdih.tulangbawangkab.go.id" target="_blank"><img src="{{url('assets/images/web/JIDH.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="http://lapor.go.id" target="_blank"><img src="{{url('assets/images/web/LAPORR.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="https://tulangbawangkab.bps.go.id/" target="_blank"><img src="{{url('assets/images/web/BPS.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#" target="_blank"><img src="{{url('assets/images/web/PIHPS.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#" target="_blank"><img src="{{url('assets/images/web/INVESTASI.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#" target="_blank"><img src="{{url('assets/images/web/E-DATA.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#" target="_blank"><img src="{{url('assets/images/web/LOWONGAN.png')}}" alt=""></a></div>
				  <div class="item hvr-wobble-vertical"> <a href="#" target="_blank"><img src="{{url('assets/images/web/KESEHATAN.png')}}" alt=""></a></div>
				 
				</div>
			  </div>
			</div>
			</div>
		  </div>
		</section>
		<section class="layer-overlay" data-bg-img="{{url('assets/images/web/bg-4.jpg')}}">
		  <div class="container">
		   <div class="section-content">
			<div class="row">
			  <div class="col-md-8">
			  <div class="row">
				  <div class="col-md-6">
					<div class="job-overview widget bg-white">
					<h5 class="widget-title line-bottom">LAYANAN NOMOR <span class="text-theme-colored">TELEPON PENTING</span> <img width="25px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/assistant.svg" title="layanan"/></h5>
					  
					  <ul class="list list-border small">
						<li><a class="text-black-222" href="telp:118">Ambulan : <span class="text-theme-colored">118</span></a></li>
						<li><a class="text-black-222" href="#">RSUD Menggala : <span class="text-theme-colored">(0726) 21118</span></a></li>
						<li><a class="text-black-222" href="#">SAR (Search And Rescue) : <span class="text-theme-colored">115</span></a></li>
						<li><a class="text-black-222" href="#">Pemadam Kebakaran : <span class="text-theme-colored">113</span></a></li>
						<li><a class="text-black-222" href="#">Dinas Pemadam Kebakaran : <span class="text-theme-colored">+628xxxxxx</span></a></li>
						<li><a class="text-black-222" href="#">PLN Pusat : <span class="text-theme-colored">123</span></a></li>
						<li><a class="text-black-222" href="#">Gangguan Telekom : <span class="text-theme-colored">117</span></a></li>
						<li><a class="text-black-222" href="#">Polisi : <span class="text-theme-colored">118</span></a></li>
						<li><a class="text-black-222" href="#">Polres Tulang Bawang : <span class="text-theme-colored">(0726) 7754121</span></a></li>
						
					  </ul>
					</div>
				  </div>
				  <div class="col-md-6">
				   <div class="widget">
					<div id="fb-root"></div>
					<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=162506317732278&autoLogAppEvents=1"></script>
					<div class="fb-page" data-href="https://www.facebook.com/puskomkreftuba/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="608" data-width="500"><blockquote cite="https://www.facebook.com/puskomkreftuba/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/puskomkreftuba/">Puskomkref Tulang Bawang</a></blockquote></div>
				    </div>
				  </div>
				  <div class="col-md-12">
				  <div class="widget">
				   <?php if(isset($data['banner']['slider footer'])){ ?>
				   <?php if(count($data['banner']['slider footer']) > 1 ) { ?>
							<div class="owl-carousel-1col" data-nav="true">
							<?php   foreach($data['banner']['slider footer'] as $item) { ?>
							<a class="img-fullwidth hvr-wobble-vertical" href="<?php echo $item['link']; ?>">
							  <div class="item"><img height="100px" src="<?php echo $item['img']; ?>" alt=""></div>
							</a>
							<?php } ?>
							</div> 
				   <?php }else{ ?>		 
							  <img height="100"  class="img-fullwidth hvr-wobble-vertical" src="<?php echo $data['banner']['slider footer']['img']; ?>" alt=""/> 
					<?php } ?>
					<?php } ?>
				  </div>
				  </div>
			  </div>
			  </div>
			  <div class="col-md-4">
				 
				<script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script> 
				<div id="gpr-kominfo-widget-container"></div>
				 
			  </div>
			</div>
		   </div>
		  </div>
		</section>
  
</div>
  <!-- end main-content -->

<script>
		$(document).ready(function(e) {
			var revapi = $(".rev_slider").revolution({
			sliderType:"standard",
			jsFileLocation: "js/revolution-slider/js/",
			sliderLayout: "auto",
			dottedOverlay: "none",
			delay: 5000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: false
				},
				arrows: {
					style: "gyges",
					enable: true,
					hide_onmobile: false,
					hide_onleave: true,
					hide_delay: 200,
					hide_delay_mobile: 1200,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 0,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 0,
						v_offset: 0
					}
				},
					bullets: {
					enable: true,
					hide_onmobile: true,
					hide_under: 800,
					style: "hebe",
					hide_onleave: false,
					direction: "horizontal",
					h_align: "center",
					v_align: "bottom",
					h_offset: 0,
					v_offset: 30,
					space: 5,
					tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
				}
			},
			responsiveLevels: [1240, 1024, 778],
			visibilityLevels: [1240, 1024, 778],
			gridwidth: [1170, 1024, 778, 480],
			gridheight: [550, 768, 960, 720],
			lazyType: "none",
			parallax:"mouse",
			parallaxBgFreeze:"off",
			parallaxLevels:[2,3,4,5,6,7,8,9,10,1],
			shadow: 0,
			spinner: "off",
			stopLoop: "on",
			stopAfterLoops: 0,
			stopAtSlide: -1,
			shuffle: "off",
			autoHeight: "off",
			fullScreenAutoWidth: "off",
			fullScreenAlignForce: "off",
			fullScreenOffsetContainer: "",
			fullScreenOffset: "0",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,
			fallbacks: {
				simplifyAll: "off",
				nextSlideOnWindowFocus: "off",
				disableFocusListener: false,
			}
			});
		});

function loadRecentpost(){
	$.ajax({
			data: {"render" : "home"},
			url: BaseUrl+"/api/news/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var left = '';
					var leftChild = '';
					var right = '';
						
					$.each(response.responseJSON.data.data.slice(0, 1), function(k,v){

						          
							left += '<article class="post mb-20">';
								left += '<a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><img class="wow zoomIn thumbnail mb-10" data-wow-duration="1.5s" data-wow-offset="10" src="'+v.real_img+'" alt=""></a>';
								left += '<div class="post-right">';
									left += '<h5 class="entry-title mt-5"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel+'</a></h5>';
									left += '<p class="post-date mb-5 font-11 font-weight-600 font-weight-600"><i class="fa fa-calendar mr-5 text-theme-colored"></i> '+v.tanggal+' <span class="ml-10"><i class="fa fa-heart-o mr-5 text-theme-colored"></i>'+v.view+' View</span><span class="ml-10"><i class="fa fa-tag mr-5 text-theme-colored"></i><a  href="'+BaseUrl+'/news/kategori/'+slugify(v.kategori)+'">'+v.kategori+'</a></span></p>';
									left += '<p class="text-justify font-13 mb-5">'+v.isi_artikel+'</p>';
                					left += '</div>';
							left += '</article>';
						 

					});
					
					$.each(response.responseJSON.data.data.slice(0, 0), function(k,v){

						          
							leftChild += '<article class="post media-post clearfix pb-1 mb-10">';
								leftChild += '<a class="post-thumb" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><div class="img-sidebar"><img class="wow fadeIn thumbnail" data-wow-duration="3s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								leftChild += '<div class="post-right">';
									leftChild += '<h5 class="post-title mt-0 mb-5"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel.substring(0,40)+'</a></h5>';
									leftChild += '<p class="post-date mb-10 font-11 font-weight-600">'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					leftChild += '</div>';
							leftChild += '</article>';

					});
					
					$.each(response.responseJSON.data.data.slice(1, 6), function(k,v){

						          
							right += '<article class="post media-post clearfix pb-1 mb-5">';
								right += '<a class="post-thumb" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><div class="img-sidebar"><img class="wow fadeIn thumbnail" data-wow-duration="3s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								right += '<div class="post-right">';
									right += '<h5 class="post-title mt-0 mb-5"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel.substring(0,40)+'</a></h5>';
									right += '<p class="post-date mb-10 font-11 font-weight-600"><a href="http://tulangbawangkab.go.id/home" target="_blank">Pemerintah Kabupaten</a><br>'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i>'+v.view+' View</span><span class="ml-10"><i class="fa fa-tag mr-5 text-theme-colored"></i><a  href="'+BaseUrl+'/news/kategori/'+slugify(v.kategori)+'">'+v.kategori+'</a></span></p>';
                					right += '</div>';
							right += '</article>';

					});

					$('#recent-left').html(left);
					$('#recent-left').append(leftChild);
					$('#recent-right').html(right);
					 
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	loadPopullar();
	};
loadRecentpost();

function loadPopullar(){
	$.ajax({
			data: {"render" : "sidebar", "type" : "popular"},
			url: BaseUrl+"/api/news/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data, function(k,v){
						content += '<div class="latest-posts">';
							content += '<article class="post media-post clearfix pb-0 mb-5">';
							content += '<a class="post-thumb" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"><div class="img-sidebar"><img class="wow fadeIn thumbnail" data-wow-duration="3s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								content += '<div class="post-right">';
								content += '<h5 class="post-title text-justify mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel.substring(0,40)+'</a></h5>';
								content += '<p class="post-date mb-10 font-11 font-weight-600">'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
								content += '</div>';
							content += '</article>';
						content += '</div>';
					});

					$('#popullar').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	loadPengumuman();
	};

	function loadPengumuman(){
	$.ajax({
			data: {"render" : "sidebar"},
			url: BaseUrl+"/api/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){

							  content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"> '+v.judul_artikel.toUpperCase()+'..</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							  content += '</article>';
					});

					$('#pengumuman-kab').html(content);
				
				}else if(response.status == 401){
						e('info','401 server conection error');
				}else{
				 	$("#btn-pengumuman-skpd").trigger("click");
				}
			},
			dataType:'json'
		})
	 
	};
	
	function loadPublikasi(){
		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/publikasi/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content +=' <li class="list-group-item hvr-shadow" style="background-color: ghostwhite;"><div class="pull-left"><i class="fa fa-download mr-10 mb-50 text-theme-colored faa-float animated float-left"></i></div><h5 class="post-title"><a class="" href="'+BaseUrl+'/informasi/publikasi/view/'+v.id+'"> '+v.judul_file+'</a></h5> <p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.created_at+'</span></p></li>';
							});
							
							$('#publikasi').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        })
	
	};
	loadPublikasi();
	function headlines(){
	$.ajax({
			data: {"render" : "sidebar", "type" : "headlines"},
			url: BaseUrl+"/api/news/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data, function(k,v){
							content += '<div class="item">';
								content += '<img class="thumbnail" src="'+v.url_img+'" alt="">';
								content += '<h4 class="title"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel+'</a></h4>';
								content += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-calendar mr-5 text-theme-colored"></i> '+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
								content += '<p class="text-justify">'+v.isi_artikel+'</p>';
							content += '</div>';
					});

					$('#headlines').html(content);
					$('.owl-carousel-headlines').each(function() {
						var data_dots = ( $(this).data("dots") === undefined ) ? false: $(this).data("dots");
						var data_nav = ( $(this).data("nav") === undefined ) ? false: $(this).data("nav");
						var data_duration = ( $(this).data("duration") === undefined ) ? 4000: $(this).data("duration");
						$(this).owlCarousel({
							 
							autoplay: true,
							autoplayTimeout: data_duration,
							loop: true,
							items: 1,
							dots: data_dots,
							nav: data_nav,
							navText: [
								'<i class="pe-7s-angle-left"></i>',
								'<i class="pe-7s-angle-right"></i>'
							]
						});
					});
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};


function loadOpd(){
	$.ajax({
			data:  {"render" : 12},
			url: ServeUrl+"api/v1/news/skpd/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var left = '',
					    right = '',
					    count = response.responseJSON.data.data.lenght;
						
					$.each(response.responseJSON.data.data.slice(0, 6), function(k,v){
							left += '<article class="post media-post clearfix pb-1 mb-10">';
								left += '<a class="post-thumb" href="'+v.link+'" target="_blank"><div class="img-sidebar"><img class="wow fadeInLeft thumbnail" data-wow-duration="1.5s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								left += '<div class="post-right">';
									left += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel.substring(0,37)+'</a></h5>';
									left += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i><a href="'+v.instansi.origin+'" target="_blank">'+v.instansi.instansi.substring(0,37)+'</a>, <br>'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					left += '</div>';
							left += '</article>';
					});
					
					$.each(response.responseJSON.data.data.slice(6, 12), function(k,v){
							right += '<article class="post media-post clearfix pb-1 mb-10">';
								right += '<a class="post-thumb" href="'+v.link+'" target="_blank"><div class="img-sidebar"><img class="wow fadeInLeft thumbnail" data-wow-duration="1.5s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								right += '<div class="post-right">';
									right += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel.substring(0,37)+'</a></h5>';
									right += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i><a href="'+v.instansi.origin+'" target="_blank">'+v.instansi.instansi.substring(0,37)+'</a>, <br>'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					right += '</div>';
							right += '</article>';
					});

					$('#opd-left').html(left);
					$('#opd-right').html(right);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};
loadOpd();

function loadKecamatan(){
	$.ajax({
			data:  {"render" : 12},
			url: ServeUrl+"api/v1/news/kecamatan/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var left = '',
					    right = '',
					    count = response.responseJSON.data.data.lenght;
						
					$.each(response.responseJSON.data.data.slice(0, 6), function(k,v){
							left += '<article class="post media-post clearfix pb-1 mb-10">';
								left += '<a class="post-thumb" href="'+v.link+'" target="_blank"><div class="img-sidebar"><img class="wow fadeInLeft thumbnail" data-wow-duration="1.5s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								left += '<div class="post-right">';
									left += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel.substring(0,37)+'</a></h5>';
									left += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i><a href="'+v.instansi.origin+'" target="_blank">'+v.instansi.instansi.substring(0,37)+'</a>, <br>'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					left += '</div>';
							left += '</article>';
					});
					
					$.each(response.responseJSON.data.data.slice(6, 12), function(k,v){
							right += '<article class="post media-post clearfix pb-1 mb-10">';
								right += '<a class="post-thumb" href="'+v.link+'" target="_blank"><div class="img-sidebar"><img class="wow fadeInLeft thumbnail" data-wow-duration="1.5s" data-wow-offset="10" src="'+v.url_img+'" alt=""></div></a>';
								right += '<div class="post-right">';
									right += '<h5 class="post-title mt-0 mb-5"><a href="'+v.link+'" target="_blank">'+v.judul_artikel.substring(0,37)+'</a></h5>';
									right += '<p class="post-date mb-10 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i><a href="'+v.instansi.origin+'" target="_blank">'+v.instansi.instansi.substring(0,37)+'</a>, <br>'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
                					right += '</div>';
							right += '</article>';
					});

					$('#kecamatan-left').html(left);
					$('#kecamatan-right').html(right);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};
loadKecamatan();

function loadUmum(){
	$.ajax({
			data: {"render" : "home"},
			url: BaseUrl+"/api/news/list/umum",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data, function(k,v){
						content += '<div class="col-xs-12 col-sm-6 col-md-3">';
								content += '<article class="post mb-30">';
										content += '<div class="entry-header">';
												content += '<div class="post-thumb">';
														content += '<img class="img-fullwidth img-responsive wow fadeInLeft thumbnail" src="'+v.url_img+'" alt="">';
											  content += '</div>';
									  content += '</div>';
										content += '<div class="clearfix"></div>';
												content += '<div class="entry-content p-10">';
												content += '<h5 class="entry-title mt-0"><a class="font-13" href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel.substring(0,37)+'..</a></h5>';
												content += '<p class="post-date mb-0 font-11 font-weight-600">'+v.tanggal+' <span class="ml-5"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> '+v.view+' View</span></p>';
											content += '<p class="mt-5 font-13 text-justify">'+v.isi_artikel.substring(0,60)+'</p>';
										content += '<div class="mt-10"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'" class="btn btn-theme-colored btn-sm">Read More</a> </div>';
										content += '</div>';
								content += '</article>';
							content += '</div>';
					});

					$('#umum').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		})
	
	};
	loadUmum();
	$('#btn-pengumuman-skpd').click(function(){
		
		$.ajax({
			data: "",
			url: ServeUrl+"/api/v1/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a target="_blank" href="'+v.link+'">'+v.judul_artikel.toUpperCase()+'</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i>'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							content += '</article>';
					});
					
					
					$('#pengumuman-skpd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		}) 
	});
	
		function loadInfoBmkg(){
		   $.ajax({
					type: "GET" ,
					url: ServeUrl+"api/v1/meta/sidebar/widget_gempa.xml" ,
					dataType: "xml" ,
					complete: function(response) {
					if(response.status == 200){
							var content = '';
							var xml = response.responseXML;
							var now = $(xml).find('gempa');
							$.each(now, function(k,v){
								content +=' <li class="list-group-item"><img class="img-responsive img-fluid" src="http://data.bmkg.go.id/eqmap.gif" alt=""></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-calendar mr-5 text-primary"></i> Tanggal : '+$(v).find('Tanggal').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-clock-o mr-5 text-primary"></i> Jam : '+$(v).find('Jam').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-industry mr-5 text-primary"></i> Magnitude : '+$(v).find('Magnitude').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-arrows-v mr-5 text-primary"></i> Kedalaman : '+$(v).find('Kedalaman').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-bullseye mr-5 text-primary"></i> Wilayah : '+$(v).find('Wilayah1').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-info mr-5 text-primary"></i> '+$(v).find('Potensi').text()+'</p></li>'; 
								 
							});
						$('#gempa-1').html(content);
						$('#gempa-1').before('<p class="font-11 p-5">sumber: <a class="text-theme-colored" href="https://www.bmkg.go.id" target="_blank">bmkg.go.id</a></p>');						
					}
					 
					}       
				});
	}
	loadInfoBmkg();
	
	$('#btn-gempa-2').click(function(){
			$.ajax({
					type: "GET" ,
					url: ServeUrl+"api/v1/meta/sidebar/widget_gempa_dirasakan.xml" ,
					dataType: "xml" ,
					complete: function(response) {
					if(response.status == 200){
							var content = '';
							var xml = response.responseXML;
							var now = $(xml).find('Gempa');
							console.log(now);
							$.each(now, function(k,v){
								 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-calendar mr-5 text-primary"></i>'+$(v).find('Tanggal').text()+'</p></li>';
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-clock-o mr-5 text-primary"></i> Jam : '+$(v).find('Jam').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-industry mr-5 text-primary"></i> Magnitude : '+$(v).find('Magnitude').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-arrows-v mr-5 text-primary"></i> Kedalaman : '+$(v).find('Kedalaman').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-bullseye mr-5 text-primary"></i> Keterangan : '+$(v).find('Keterangan').text()+'</p></li>'; 
								content +=' <li class="list-group-item"><p class="post-date m-0 font-12 font-weight-600"><i class="fa fa-rss mr-5 text-primary"></i> Dirasakan : '+$(v).find('Dirasakan').text()+'</p></li>'; 
								 
							});
						$('#gempa-2').html(content);	
					}
					 
					}       
			});
	});
	function slugify(string) {
	  if(string){
	  return string
		.toString()
		.trim()
		.toLowerCase()
		.replace(/\s+/g, "-")
		.replace(/[^\w\-]+/g, "")
		.replace(/\-\-+/g, "-")
		.replace(/^-+/, "")
		.replace(/-+$/, "");
	  }
	}
</script>

@stop
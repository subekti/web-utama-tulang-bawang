@extends('frontend.body')
@section('content')
<div class="main-content">
         <!-- Page title section start -->

        <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="assets/images/web/Bupati-dan-Wakil-Bupati-Tulang-Bawang.jpg">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
            <div class="row">
                <div class="col-md-12 text-center">
                <h2 class="title">Gallery</h2>
                <ol class="breadcrumb text-center text-black mt-10">
                    <li><a href="#">Home</a></li>
                    <li class="active text-theme-colored">{{$data['title']}}</li>
                </ol>
                </div>
            </div>
            </div>
        </div>
        </section>
        <!-- Page title section end -->
        
		 <!-- Portfolio section start -->
         <section class="divider  bg-white">
                <div class="container">
                  <div class="section-content">
                    <div class="row">
                      <div class="col-md-12">
                        <!-- Portfolio Filter -->
                        <div class="portfolio-filter text-center mb-40" data-link-with="petcare-gallery">
						  <a href="#" class="active" data-filter="*">All</a>
						  <a href="#image" class="" data-filter=".image">Pictures</a>
						  <a href="#embed" class="" data-filter=".embed">Videos</a>
						</div>
                        <!-- End Portfolio Filter -->
          
                        <!-- Portfolio Gallery Grid -->
                        <div class="gallery-isotope grid-4 gutter-small clearfix" data-lightbox="gallery" id="gallery-list">

                        </div>
                        <!-- End Portfolio Gallery Grid -->
                         <div class="text-center"><div class="loader text-center"></div><a id="loadmore" class="btn btn-colored btn-flat btn-theme-colored hvr-overline-from-center mt-15 pr-40 pl-40" onclick="loadMore()" data-value=""><strong><i class="fa fa-circle-o-notch"></i> Load More</strong></a></div>   
                      </div>
                    </div>
                  </div>
                </div>
              </section>
        <!-- Portfolio section end -->   
        
    <script src="{{url('assets/frontend')}}/js/jquery.isotope.min.js"></script>
</div>	    
    
    <script>
	var $portfolio_gallery = $(".gallery-isotope");
	var isotope_mode ="masonry";
	var data 	= {"page" : 1};
	function loadGallery(data){
		
		$.ajax({
					data: data,
					url: BaseUrl+"/api/gallery/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
                                content += '<div class="gallery-item '+v.type+'">';
                                    content += '<div class="thumb">';
									if(v.type == 'image'){
										content += '<img class="img-fullwidth" src="'+v.img_link+'" alt="project">';
										content += '<div class="overlay-shade"></div>';
										content += '<div class="text-holder">';
											content += ' <div class="title text-center">'+v.judul_media+'</div>';
										content += '</div>';
										content += '<div class="icons-holder">';
											content += '<div class="icons-holder-inner">';
											content += '<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">';
											content += ' <a href="'+v.img_link+'" data-lightbox-gallery="gallery" title="'+v.judul_media+'"><i class="fa fa-picture-o"></i></a>';
											content += '</div>';
											content += '</div>';
										content += '</div>';
									}else if(v.type == 'embed'){
										var youtube = $(v.source).html(content);
										 
										content += '<div class="embed-responsive embed-responsive-16by9">'+v.source+'</div>';
										content += '<div class="overlay-shade"></div>';
											content += '<div class="icons-holder">';
											content += '<div class="icons-holder-inner">';
												content += '<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">';
												  content += '<a data-lightbox-gallery="gallery" href="https://www.youtube.com/watch?v='+youtube.attr('src').split("/").pop()+'"><i class="fa fa-youtube-play"></i></a>';
												content += '</div>';
											content += '</div>';
										content += '</div>';
										 
									};
                                        

                                    content += '</div>';
                                content += '</div>';
							});
							$('#loadmore').data("value", response.responseJSON.data.current_page);
							$('#gallery-list').append(content);
							
							isotop(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 404){
							 $('#loadmore').remove();
							 $('#gallery-list').after('<center class="mt-5 m-b-50"><h4>Oops! Not Found</h4></center>');
						}
                    },
					dataType:'json'
        })
	
	};
	loadGallery(data);
	
	function loadMore(){
		var page = parseInt($('#loadmore').data("value"))+1;
		var data = {"page" : page};
		loadGallery(data);		
	};
	
	function isotop(content){
	     
            $portfolio_gallery.imagesLoaded(function(){
                $portfolio_gallery.isotope({
                    itemSelector: '.gallery-item',
                    layoutMode: isotope_mode,
                    filter: "*"
                });
                $portfolio_gallery.isotope( 'reloadItems' ).isotope();
            });
            
            $('a[data-lightbox-gallery]').nivoLightbox({
                effect: 'fadeScale'
            });
	}
	
	
	
	$(".portfolio-filter").each(function () {
		var $portfolio_filter = $(this).children('a');
		var linkwith = $(this).data('link-with');
		$portfolio_filter.on("click", function(){
			$portfolio_filter.removeClass("active");
			$(this).addClass("active");
			var fselector = $(this).data('filter');
			$portfolio_gallery.isotope({
				itemSelector: '.gallery-item',
				layoutMode: isotope_mode,
				filter: fselector
			});
			return false;
		});
    });
	</script>  
@stop
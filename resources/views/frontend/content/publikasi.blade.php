@extends('frontend.body')
@section('content')
<div class="main-content">
         <!-- Page title section start -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">{{$data['title']}}</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active text-theme-colored">{{$data['title']}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
	
        <!-- Blog page section start -->
        <section id="schedule" class="divider bg-white">
            <div class="container pt-80 pb-60">
              <div class="section-content">
                <div class="row">
                  <div class="col-md-9 pull-right flip sm-pull-none">
				  <div class="border-1px p-20 pr-10">
				   <div class="table-responsive">
					<div class="widget">
					 
					  <div class="search-form">
						<form action="{{url('/informasi/publikasi')}}">
						  <div class="input-group">
							<input name="keyword" type="text" placeholder="Click to Search" class="form-control search-input">
							<span class="input-group-btn">
							<button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
							</span>
						  </div>
						</form>
					  </div>
					</div>
				
                    <table class="table table-striped table-schedule table-hover">
                      <thead>
                        <tr class="bg-theme-colored">
                          <th style="text-align:center">Judul</th>
                          <th style="text-align:center">Type</th>
                          <th style="text-align:center">Upload Date</th>
                        </tr>
                      </thead>
                      <tbody id="files" style="text-align:center">
    
                      </tbody>
                    </table>
					
					<div class="text-center mb-10"><div class="loader text-center"></div><a id="loadmore" class="btn btn-colored btn-flat btn-theme-colored hvr-overline-from-center mt-15 pr-40 pl-40" onclick="loadMore()" data-value=""><strong><i class="fa fa-circle-o-notch"></i> Load More</strong></a></div>
					<i class="small">*) klik pada kolom judul untuk melihat informasi detail</i>
				   </div>
                  </div>
                  </div>
                  <div class="col-md-3">
					<div class="sidebar sidebar-right mt-sm-30">
					 
					<div class="widget">
						<h4 class="widget-title line-bottom">
							<span>INFO <span class="text-theme-colored">PENGUMUMAN</span></span> 
							<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/pengumuman')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"></a></span>
						</h4>
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" href="#pengumuman-kab" aria-expanded="true">KABUPATEN</a></li>
							<li class=""><a data-toggle="tab" href="#pengumuman-skpd" id="btn-pengumuman-skpd" aria-expanded="false">SKPD</a></li>
						</ul>
						<div class="tab-content">
							<div id="pengumuman-kab" class="tab-pane fade active in">
								<div class="loader text-center"></div>
							</div>
							<div id="pengumuman-skpd" class="tab-pane fade">
								<div class="loader text-center"></div>
							</div>
						</div>
					</div> 
					
					<div class="widget">
						<h4 class="widget-title line-bottom">
							<span>PUBLIKASI <span class="text-theme-colored">OPD</span></span>
							<span class="pull-right"><a class="text-theme-colored" href="#"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/opened_folder.svg" title="Selengkapnya"></a></span>
						</h4>
						<ul id="publikasi-opd" class="list-group">
								<div class="loader text-center"></div>
						</ul>
					</div>
					
					<?php if(isset($data['banner']['sidebar'])){   ?>
					<div class="widget">
					<h4 class="widget-title line-bottom">
							<span>THE <span class="text-theme-colored">CORNER</span></span>
							<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/frame.svg" title="Selengkapnya"/></span> 
					</h4>
						<img class="img-fullwidth" src="<?php echo $data['banner']['sidebar']['img']; ?>" alt="">
					</div>
					<?php } ?>
					</div>
				  </div>
        
				</div>
              </div>
            </div>
          </section>
        <!-- Blog page section end -->
 </div>  
   <script>
	var page = {"page" : 1};
	var extend  = getUrlVars();
	var data 	= $.extend(extend, page);
	function loadDownloads(data){
		if(getUrlVars().keyword){ $('input[name=keyword]').val(getUrlVars().keyword); }
		$.ajax({
					data: data,
					url: BaseUrl+"/api/publikasi/list",
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = ''; 
							$.each(response.responseJSON.data.data, function(k,v){
                                content += '<tr>'
                                content += '<td><a href="{{url("/informasi/publikasi/view/")}}/'+v.id+'">'+v.judul_file+'</a></td>';
                                content += '<td>'+v.icon+' '+v.type_file+'</td>';
                                content += '<td>'+v.created_at+'</td>';
								                content +='<li> ';
                                    content +='';
                                content +='</li> ';
                                content +='</tr> ';
							
							});
							$('#loadmore').data("value", response.responseJSON.data.current_page);
							$('#files').append(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 404){
							 $('#loadmore').remove();
							 $('.table-schedule').after('<center class="m-t-50 m-b-50"><h4>Oops! Not Found</h4></center>');
						}
                    },
					dataType:'json'
        })
	loadSidebar();
	};
	
	loadDownloads(data);
	function loadPengumuman(){
	$.ajax({
			data: {"render" : "sidebar"},
			url: BaseUrl+"/api/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){

							  content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'"> '+v.judul_artikel.toUpperCase()+'..</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							  content += '</article>';
					});

					$('#pengumuman-kab').html(content);
				
				}else if(response.status == 401){
						e('info','401 server conection error');
				}else{
				 	$("#btn-pengumuman-skpd").trigger("click");
				}
			},
			dataType:'json'
		})
	 
	};
	loadPengumuman();
	function loadSidebar(){
		
		$.ajax({
					data: {"render" : "sidebar"},
					url: ServeUrl+"api/v1/publikasi/skpd/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
							content +=' <li class="list-group-item hvr-shadow"><h5 class="post-title"><a target="_blank" href="'+v.link+'"><i class="fa fa-download mr-5 text-primary faa-float animated"></i> '+v.judul_file.substring(0,25).toUpperCase()+'</a></h5> <p class="post-date ml-20 font-11 font-weight-600">'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.date+'</span></p></li>';
							});
							
							$('#publikasi-opd').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        })
	
	};

$('#btn-pengumuman-skpd').click(function(){
		
		$.ajax({
			data: "",
			url: ServeUrl+"/api/v1/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a target="_blank" href="'+v.link+'">'+v.judul_artikel.toUpperCase()+'</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i>'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							content += '</article>';
					});
					
					
					$('#pengumuman-skpd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		}) 
	});

	function loadMore(){
		var page = parseInt($('#loadmore').data("value"))+1;
		var data = {"page" : page};
		loadDownloads(data);		
	};
	</script>  
@stop
@extends('frontend.body')
@section('content')
<div class="main-content">
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="{{url('assets/images/web/free-quote-bg.jpg')}}">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-sm-8 text-left flip xs-text-center">
              <h2 class="title">{{$data['title']}}</h2>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb text-right sm-text-center text-black mt-10">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active text-theme-colored">{{$data['title']}}</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
	
    <section class="divider  bg-white">
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-md-9 pull-right flip sm-pull-none">
            <div class="blog-posts">
              <div class="col-md-12">
                <div class="row list-dashed">
                  <article class="post clearfix mb-30 pb-30">
                    
                    <div class="entry-content border-1px p-20 pr-10" id="blog-content">
                       
                      <div class="clearfix"></div>
                    </div>
                  </article>
				</div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h4 class="widget-title line-bottom">
					<span>INFORMASI <span class="text-theme-colored">LAINYA</span></span> 
					<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/info.svg" title="Selengkapnya"/></span> 
				</h4>
                <ul class="list-divider list-border list check" id="page-list">

                </ul>
              </div>
            <div class="widget">
				<h4 class="widget-title line-bottom">
					<span>INFO <span class="text-theme-colored">PENGUMUMAN</span></span> 
					<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/pengumuman')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/advertising.svg" title="Selengkapnya"></a></span>
				</h4>
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a data-toggle="tab" href="#pengumuman-kab" aria-expanded="true">KABUPATEN</a></li>
					<li class=""><a data-toggle="tab" href="#pengumuman-skpd" id="btn-pengumuman-skpd"aria-expanded="false">SKPD</a></li>
				</ul>
				<div class="tab-content">
					<div id="pengumuman-kab" class="tab-pane fade active in">
						<div class="loader text-center"></div>
					</div>
					<div id="pengumuman-skpd" class="tab-pane fade">
						<div class="loader text-center"></div>
					</div>
				</div>
			</div> 
			
			<div class="widget">
				<h4 class="widget-title line-bottom">
					<span>INFO <span class="text-theme-colored">PUBLIKASI</span></span> 
					<span class="pull-right"><a class="text-theme-colored" href="{{url('news/kategori/publikasi')}}"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/opened_folder.svg" title="Selengkapnya"></a></span>
				</h4>
				<ul id="publikasi" class="list-group">
						<div class="loader text-center"></div>
				</ul>
			</div>
			<?php if(isset($data['banner']['sidebar'])){   ?>
						<div class="widget mb-5">
						    <h4 class="widget-title line-bottom">
									<span>INFO <span class="text-theme-colored">SOSIALISASI</span></span>
									<span class="pull-right"><img width="23px" alt="" src="{{url('assets/frontend')}}/images/flat-color-icons-svg/frame.svg" title="Selengkapnya"/></span> 					
							</h4>
							<?php if($data['banner']['sidebar']['link'] != ''){ ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar']['link']; ?>">
							<?php } else { ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar']['img']; ?>" data-lightbox-gallery="gallery" title="<?php echo $data['banner']['sidebar']['keterangan']; ?>">
							<?php } ?>
							<img class="img-fullwidth thumbnail" src="<?php echo $data['banner']['sidebar']['img']; ?>" alt="">
							</a>
						 </div>
						 <?php } ?>
						 <?php if(isset($data['banner']['sidebar2'])){   ?>
						<div class="widget">
							<?php if($data['banner']['sidebar2']['link'] != ''){ ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar2']['link']; ?>">
							<?php } else { ?>
							<a class="hvr-grow" href="<?php echo $data['banner']['sidebar2']['img']; ?>" data-lightbox-gallery="gallery" title="<?php echo $data['banner']['sidebar2']['keterangan']; ?>">
							<?php } ?>
							<img class="img-fullwidth thumbnail" src="<?php echo $data['banner']['sidebar2']['img']; ?>" alt="">
							</a>
						 </div>
						 <?php } ?>
			</div>
          </div>
        </div>
      </div>
    </section> 
</div>  
	<script>
	function loadView(){
		var page = window.location.pathname.split('/').pop();
		$.ajax({
					data: "",
					url: BaseUrl+"/api/pages/view/"+page,
                    crossDomain: true,
                    method: 'GET',
                    complete: function(response){ 		
                        if(response.status == 200){
							
							 $("#blog-content").html(response.responseJSON.data.content); 
							 
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}else if(response.status == 204){ 
							 $("#blog-content").html('<center class="m-t-50 m-b-50"><h2>Oops! Not Found</h2></center>');
						}
                    },
					dataType:'json'
                })
                loadSidebar();
	}
	function loadPengumuman(){
	$.ajax({
			data: {"render" : "sidebar"},
			url: BaseUrl+"/api/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){

							  content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a href="'+BaseUrl+'/news/read/'+v.id+'/'+v.slug+'">'+v.judul_artikel.substring(0,24).toUpperCase()+'..</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							  content += '</article>';
					});

					$('#pengumuman-kab').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}else{
				$("#btn-pengumuman-skpd").trigger("click");	 
				}
			},
			dataType:'json'
		})
	};
	loadPengumuman();
	function loadSidebar(){
		var page = window.location.pathname.split('/').pop();
		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/pages/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
							var addClass= 'class="text-black-555 font-13"'; if(v.slug == page){ addClass = 'class="active text-black-555 font-13"';};
									content +='<li '+addClass+'>';
                                        content +='<a '+addClass+' href="'+BaseUrl+'/informasi/'+v.slug+'">';
                                            content += v.judul;
                                        content +='</a>';
                                    content +='</li>';
							
							});
							
							$('#page-list').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
                });
				
		$.ajax({
					data: {"render" : "sidebar"},
					url: BaseUrl+"/api/publikasi/list",
                    
                    method: 'GET',
                    complete: function(response){ 				
                        if(response.status == 200){
							var content = '';
							 
							$.each(response.responseJSON.data.data, function(k,v){
						content +=' <li class="list-group-item hvr-shadow"><div class="pull-left"><i class="fa fa-download mr-10 mb-50 text-theme-colored faa-float animated float-left"></i></div><h5 class="post-title"><a class="" href="'+BaseUrl+'/informasi/publikasi/view/'+v.id+'"> '+v.judul_file+'</a></h5> <p class="post-date ml-20 font-11 font-weight-600"> Published on : <span class="text-theme-colored">'+v.created_at+'</span></p></li>';
							});
							
							$('#publikasi').html(content);
                        }else if(response.status == 401){
							 e('info','401 server conection error');
						}
                    },
					dataType:'json'
        })
	
	};
	loadView();
	
	$('#btn-pengumuman-skpd').click(function(){
		$.ajax({
			data: "",
			url: ServeUrl+"/api/v1/news/kategori/pengumuman/list",
			
			method: 'GET',
			complete: function(response){ 				
				if(response.status == 200){
					var content = '';
						
					$.each(response.responseJSON.data.data.slice(0, 5), function(k,v){
							content += '<article class="post media-post clearfix pb-0">';
								content += '<div class="post-right"><div class="pull-left"><i class="fa fa-bullhorn mr-10 mb-50 text-theme-colored faa-flash animated"></i></div>';
									content += '<h5 class="post-title mt-0"><a target="_blank" href="'+v.link+'">'+v.judul_artikel.toUpperCase()+'</a></h5>';
									content += '<p class="post-date ml-20 font-11 font-weight-600"><i class="fa fa-building-o mr-5"></i>'+v.instansi.instansi.substring(0,31)+'<br> Published on : <span class="text-theme-colored">'+v.tanggal+'</span></p>';
								content += '</div>';
							content += '</article>';
					});
					
					
					$('#pengumuman-skpd').html(content);
					
				}else if(response.status == 401){
						e('info','401 server conection error');
				}
			},
			dataType:'json'
		}) 	
	});
</script>	
@stop